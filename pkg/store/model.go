// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"time"

	"github.com/google/uuid"
	"gorm.io/datatypes"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

type AreaPoint struct {
	Id     uuid.UUID `gorm:"primaryKey;type:uuid" json:"id"`
	AreaId uuid.UUID `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	X      float64   `json:"x" binding:"required"`
	Y      float64   `json:"y" binding:"required"`
	Z      float64   `json:"z"`
}

func (x *AreaPoint) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Area struct {
	Id          uuid.UUID   `gorm:"primaryKey;type:uuid" json:"id"`
	ElementId   uuid.UUID   `gorm:"type:uuid" json:"element_id" binding:"required"`
	Name        string      `gorm:"size:256" json:"name" binding:"required"`
	Description string      `gorm:"size:256" json:"description"`
	Srid        int         `json:"srid" binding:"required"`
	Points      []AreaPoint `json:"points"`
}

func (x *Area) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Annotation struct {
	Id            uuid.UUID      `gorm:"primaryKey;type:uuid" json:"id"`
	ObservationId uuid.UUID      `gorm:"foreignKey:ObservationId;type:uuid" json:"observation_id" binding:"required"`
	Type          string         `gorm:"size:512" json:"type" binding:"required"`
	Name          string         `gorm:"size:512" json:"name" binding:"required"`
	Description   string         `gorm:"size:4096" json:"description"`
	Value         datatypes.JSON `json:"value"`
	CreatorId     uuid.UUID      `gorm:"type:uuid" json:"creator_id"`
	UpdaterId     *uuid.UUID     `gorm:"type:uuid" json:"updater_id"`
	CreatedAt     time.Time      `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt     *time.Time     `gorm:"autoUpdateTime" json:"updated_at"`
}

func (x *Annotation) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Observation struct {
	Id                     uuid.UUID    `gorm:"primaryKey;type:uuid" json:"id"`
	MonitorId              uuid.UUID    `gorm:"type:uuid" json:"monitor_id" binding:"required"`
	CollectionId           *uuid.UUID   `gorm:"foreignKey:CollectionId;type:uuid" json:"collection_id"`
	Types                  string       `gorm:"size:512" json:"types" binding:"required"`
	Format                 string       `gorm:"size:512" json:"format" binding:"required"`
	Description            string       `gorm:"size:4096" json:"description"`
	MinFreq                int64        `json:"min_freq"`
	MaxFreq                int64        `json:"max_freq"`
	FreqStep               int64        `json:"freq_step"`
	StartsAt               time.Time    `json:"starts_at" binding:"required"`
	EndsAt                 *time.Time   `json:"ends_at" binding:"omitempty,gtfield=StartsAt"`
	Data                   []byte       `json:"data" binding:"required"`
	Violation              bool         `json:"violation"`
	ViolationVerifiedAt    *time.Time   `json:"violation_verified_at"`
	Interference           bool         `json:"interference"`
	InterferenceVerifiedAt *time.Time   `json:"interference_verified_at"`
	ElementId              uuid.UUID    `gorm:"type:uuid" json:"element_id"`
	CreatorId              uuid.UUID    `gorm:"type:uuid" json:"creator_id"`
	UpdaterId              *uuid.UUID   `gorm:"type:uuid" json:"updater_id"`
	CreatedAt              time.Time    `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt              *time.Time   `gorm:"autoUpdateTime" json:"updated_at"`
	DeletedAt              *time.Time   `gorm:"autoUpdateTime" json:"deleted_at"`
	Annotations            []Annotation `json:"annotations"`
	ViolatedGrantId        *uuid.UUID   `gorm:"type:uuid" json:"violated_grant_id" binding:"-"`
	Path                   string       `gorm:"size:512" json:"-" binding:"-"`
}

func (x *Observation) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type Collection struct {
	Id              uuid.UUID     `gorm:"primaryKey;type:uuid" json:"id"`
	MonitorId       uuid.UUID     `gorm:"type:uuid" json:"monitor_id" binding:"required"`
	Types           string        `gorm:"size:512" json:"types" binding:"required"`
	Format          string        `gorm:"size:512" json:"format" binding:"required"`
	Description     string        `gorm:"size:4096" json:"description"`
	MinFreq         int64         `json:"min_freq"`
	MaxFreq         int64         `json:"max_freq"`
	StartsAt        time.Time     `json:"starts_at" binding:"required"`
	EndsAt          *time.Time    `json:"ends_at" binding:"omitempty,gtfield=StartsAt"`
	Violation       bool          `json:"violation"`
	Interference    bool          `json:"interference"`
	ElementId       uuid.UUID     `gorm:"type:uuid" json:"element_id"`
	CreatorId       uuid.UUID     `gorm:"type:uuid" json:"creator_id"`
	UpdaterId       *uuid.UUID    `gorm:"type:uuid" json:"updater_id"`
	CreatedAt       time.Time     `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt       *time.Time    `gorm:"autoUpdateTime" json:"updated_at"`
	Observations    []Observation `json:"observations"`
	ViolatedGrantId *uuid.UUID    `gorm:"type:uuid" json:"violated_grant_id" binding:"-"`
}

func (x *Collection) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimOutputType string

const (
	Raw     PropsimOutputType = "raw"
	GeoTIFF PropsimOutputType = "geotiff"
)

func CreatePropsimOutputTypeEnum(db *gorm.DB) (err error) {
	res := db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_output_type_enum AS ENUM ('raw', 'geotiff');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

func (x *PropsimOutputType) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid PropsimOutputType value")
	}
	*x = PropsimOutputType(s)
	return nil
}

func (x PropsimOutputType) Value() (interface{}, error) {
	return string(x), nil
}

func (PropsimOutputType) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "propsim_output_type_enum"
	}
	return ""
}

type PropsimOutputKind string

const (
	UnknownKind PropsimOutputKind = "unknown"
	Pathloss    PropsimOutputKind = "pathloss"
	Power       PropsimOutputKind = "power"
	LineOfSight PropsimOutputKind = "los"
	OtherKind   PropsimOutputKind = "other"
)

func CreatePropsimOutputKindEnum(db *gorm.DB) (err error) {
	res := db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_output_kind_enum AS ENUM ('unknown', 'pathloss', 'power', 'los', 'other');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

func ParsePropsimOutputKind(x string) (PropsimOutputKind, error) {
	if x == "unknown" {
		return UnknownKind, nil
	} else if x == "pathloss" {
		return Pathloss, nil
	} else if x == "power" {
		return Power, nil
	} else if x == "los" {
		return LineOfSight, nil
	} else if x == "other" {
		return OtherKind, nil
	} else {
		return UnknownKind, errors.New("unrecognized PropsimOutputKind")
	}
}

func (x *PropsimOutputKind) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid PropsimOutputKind value")
	}
	*x = PropsimOutputKind(s)
	return nil
}

func (x PropsimOutputKind) Value() (interface{}, error) {
	return string(x), nil
}

func (PropsimOutputKind) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "propsim_output_kind_enum"
	}
	return ""
}

type PropsimOutputUnit string

const (
	UnknownUnit PropsimOutputUnit = "unknown"
	dB          PropsimOutputUnit = "dB"
	dBm         PropsimOutputUnit = "dBm"
	mW          PropsimOutputUnit = "mW"
	LOS         PropsimOutputUnit = "LOS"
	OtherUnit   PropsimOutputUnit = "other"
)

func ParsePropsimOutputUnit(x string) (PropsimOutputUnit, error) {
	if x == "unknown" {
		return UnknownUnit, nil
	} else if x == "dB" {
		return dB, nil
	} else if x == "dBm" {
		return dBm, nil
	} else if x == "mW" {
		return mW, nil
	} else if x == "LOS" {
		return LOS, nil
	} else if x == "other" {
		return OtherUnit, nil
	} else {
		return UnknownUnit, errors.New("unrecognized PropsimOutputUnit")
	}
}

func CreatePropsimOutputUnitEnum(db *gorm.DB) (err error) {
	res := db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_output_unit_enum AS ENUM ('unknown', 'dB', 'dBm', 'mW', 'LOS', 'other');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

func (x *PropsimOutputUnit) Scan(value interface{}) error {
	s, ok := value.(string)
	if !ok {
		return errors.New("invalid PropsimOutputUnit value")
	}
	*x = PropsimOutputUnit(s)
	return nil
}

func (x PropsimOutputUnit) Value() (interface{}, error) {
	return string(x), nil
}

func (PropsimOutputUnit) GormDBDataType(db *gorm.DB, field *schema.Field) string {
	switch db.Dialector.Name() {
	case "sqlite":
		return "text"
	case "postgres":
		return "propsim_output_unit_enum"
	}
	return ""
}

type PropsimRaster struct {
	Rid  int    `gorm:"type:serial;primaryKey;autoIncrement" json:"-"`
	Rast []byte `gorm:"type:raster" json:"-"`
}

func CreatePropsimRasterIdx(db *gorm.DB) (err error) {
	res := db.Debug().Exec(`
    DO $$ BEGIN
        CREATE INDEX IF NOT EXISTS "propsim_rasters_st_convexhull_idx" ON "propsim_rasters" USING gist (st_convexhull("rast"));
    END $$;`)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type PropsimJobOutput struct {
	Id                 uuid.UUID         `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimJobId       uuid.UUID         `gorm:"foreignKey:PropsimJobId;type:uuid;uniqueIndex:idx_propsim_job_id_job_output_id" json:"propsim_job_id"`
	JobOutputId        *uuid.UUID        `gorm:"type:uuid;uniqueIndex:idx_propsim_job_id_job_output_id" json:"job_output_id"`
	Type               PropsimOutputType `sql:"type:propsim_output_type_enum" json:"type"`
	Kind               PropsimOutputKind `sql:"type:propsim_output_kind_enum" json:"kind"`
	Unit               PropsimOutputUnit `sql:"type:propsim_output_unit_enum" json:"unit"`
	Name               string            `json:"name"`
	Desc               *string           `json:"desc"`
	Message            *string           `gorm:"size:1024" json:"message"`
	Size               uint64            `json:"size"`
	Path               string            `json:"-"`
	PropsimRasterRid   *int              `gorm:"foreignKey:PropsimRasterRid;references:Rid" json:"-"`
	GeoserverWorkspace *string           `json:"geoserver_workspace"`
	GeoserverLayer     *string           `json:"geoserver_layer"`
	AreaId             *uuid.UUID        `gorm:"foreignKey:AreaId;type:uuid" json:"area_id"`
	Area               *Area             `json:"area"`
	CreatedAt          time.Time         `gorm:"autoCreateTime" json:"created_at"`
	DeletedAt          *time.Time        `json:"deleted_at"`
}

type PropsimStatus string

const (
	Unknown    PropsimStatus = "unknown"
	Created    PropsimStatus = "created"
	Scheduled  PropsimStatus = "scheduled"
	Running    PropsimStatus = "running"
	Processing PropsimStatus = "processing"
	Complete   PropsimStatus = "completed"
	Failed     PropsimStatus = "failed"
	Deleted    PropsimStatus = "deleted"
)

func CreatePropsimStatusEnum(db *gorm.DB) (err error) {
	res := db.Debug().Exec(`
	DO $$ BEGIN
		CREATE TYPE propsim_status_enum AS ENUM ('created', 'scheduled', 'running', 'processing', 'completed', 'failed', 'deleted');
	EXCEPTION
		WHEN duplicate_object THEN null;
	END $$;`)
	if res.Error != nil {
		return res.Error
	}
	return nil
}

type PropsimJob struct {
	Id         uuid.UUID          `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimId  uuid.UUID          `gorm:"foreignKey:PropsimId;type:uuid;uniqueIndex:idx_propsim_id_service_id_job_id" json:"propsim_id"`
	ServiceId  uuid.UUID          `gorm:"type:uuid;uniqueIndex:idx_propsim_id_service_id_job_id" json:"service_id"`
	JobId      *uuid.UUID         `gorm:"type:uuid;uniqueIndex:idx_propsim_id_service_id_job_id" json:"job_id"`
	Status     PropsimStatus      `sql:"type:propsim_status_enum" json:"status"`
	Message    *string            `gorm:"size:1024" json:"message"`
	CreatedAt  time.Time          `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt  *time.Time         `gorm:"autoUpdateTime" json:"updated_at"`
	FinishedAt *time.Time         `json:"finished_at"`
	DeletedAt  *time.Time         `json:"deleted_at"`
	Progress   float64            `json:"progress"`
	Outputs    []PropsimJobOutput `json:"outputs"`
}

func (x *PropsimJob) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

// XXX: need to "connect" the radio/antenna/location info for each tx/rx from the zmc and high-level api
// given a tx radio id, power, what are common params?  what are sim-specific?  how to handle data type conv?  maybe marshal into propsim messages immediately; if fail, return sync error to API caller

// Eventually? per-radio port/site settings: PropsimRadio{radio_port,gain,config}

/*
type PropsimRadioPort struct {
	Id          uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimId  uuid.UUID          `gorm:"foreignKey:PropsimId;type:uuid;uniqueIndex:idx_propsim_id_service_id_job_id" json:"propsim_id"`
	RadioPortId *uuid.UUID `gorm:"type:uuid" json:"radio_port_id"`
	Gain        float32    `json:"gain"`

}
*/

type Propsim struct {
	Id          uuid.UUID       `gorm:"primaryKey;type:uuid" json:"id"`
	Name        string          `gorm:"size:256" json:"name" binding:"required,max=256"`
	Description *string         `gorm:"size:1024" json:"description" binding:"omitempty,max=1024"`
	RadioPortId *uuid.UUID      `gorm:"type:uuid" json:"radio_port_id" binding:"required"`
	RadioId     *uuid.UUID      `gorm:"type:uuid" json:"radio_id"`
	Freq        *int64          `json:"freq"`
	Gain        *float32        `json:"gain"`
	RxHeight    *float32        `json:"rx_height"`
	Resolution  *float32        `json:"resolution"`
	Config      *datatypes.JSON `json:"config"`
	ServiceId   *uuid.UUID      `gorm:"type:uuid" json:"service_id" binding:"required"`
	Priority    int             `json:"priority"`
	CreatorId   uuid.UUID       `gorm:"type:uuid" json:"creator_id"`
	ElementId   uuid.UUID       `gorm:"type:uuid" json:"element_id"`
	Status      PropsimStatus   `sql:"type:propsim_status_enum" json:"status"`
	CreatedAt   time.Time       `gorm:"autoCreateTime" json:"created_at"`
	UpdatedAt   *time.Time      `gorm:"autoUpdateTime" json:"updated_at"`
	FinishedAt  *time.Time      `json:"finished_at"`
	DeletedAt   *time.Time      `json:"deleted_at"`
	Jobs        []PropsimJob    `json:"jobs"`
}

func (x *Propsim) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimPowerGrant struct {
	Id                 uuid.UUID         `gorm:"primaryKey;type:uuid" json:"id"`
	GrantId            uuid.UUID         `gorm:"type:uuid;uniqueIndex:idx_grant_id" json:"grant_id"`
	Size               uint64            `json:"size"`
	Path               string            `json:"-"`
	RasterRid          *int              `gorm:"foreignKey:RasterRid;references:Rid" json:"-"`
	GeoserverWorkspace *string           `json:"geoserver_workspace"`
	GeoserverLayer     *string           `json:"geoserver_layer"`
	CreatedAt          time.Time         `gorm:"autoCreateTime" json:"created_at"`
	FinishedAt         *time.Time        `json:"finished_at"`
	DeletedAt          *time.Time        `json:"deleted_at"`
	JobOutputs         []PropsimPowerGrantJobOutput `json:"job_outputs"`
}

func (x *PropsimPowerGrant) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimPowerGrantJobOutput struct {
	Id                  uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimPowerGrantId uuid.UUID  `gorm:"foreignKey:PropsimPowerGrantId;type:uuid;uniqueIndex:idx_propsim_power_grant_id_propsim_job_output_id" json:"propsim_power_grant_id"`
	PropsimJobOutputId  uuid.UUID  `gorm:"foreignKey:PropsimJobOutputId;type:uuid;uniqueIndex:idx_propsim_power_grant_id_propsim_job_output_id" json:"propsim_job_output_id"`
	PropsimJobOutput    *PropsimJobOutput `json:"propsim_job_output"`
}

func (x *PropsimPowerGrantJobOutput) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimPowerGlobal struct {
	Id                 uuid.UUID         `gorm:"primaryKey;type:uuid" json:"id"`
	Size               uint64            `json:"size"`
	Path               string            `json:"-"`
	RasterRid          *int              `gorm:"foreignKey:RasterRid;references:Rid" json:"-"`
	GeoserverWorkspace *string           `json:"geoserver_workspace"`
	GeoserverLayer     *string           `json:"geoserver_layer"`
	CreatedAt          time.Time         `gorm:"autoCreateTime" json:"created_at"`
	FinishedAt         *time.Time        `json:"finished_at"`
	DeletedAt          *time.Time        `json:"deleted_at"`
}

func (x *PropsimPowerGlobal) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

type PropsimPowerGlobalGrant struct {
	Id                  uuid.UUID  `gorm:"primaryKey;type:uuid" json:"id"`
	PropsimPowerGlobalId uuid.UUID  `gorm:"foreignKey:PropsimPowerGlobalId;type:uuid;uniqueIndex:idx_propsim_power_global_id_propsim_power_grant_id" json:"propsim_power_global_id"`
	PropsimPowerGrantId uuid.UUID  `gorm:"foreignKey:PropsimPowerGrantId;type:uuid;uniqueIndex:idx_propsim_power_global_id_propsim_power_grant_id" json:"propsim_power_grant_id"`
}

func (x *PropsimPowerGlobalGrant) BeforeCreate(db *gorm.DB) error {
	if x.Id == uuid.Nil {
		x.Id = uuid.New()
	}
	return nil
}

