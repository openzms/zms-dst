// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"

	"google.golang.org/protobuf/types/known/timestamppb"

	dst_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	propsim_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/propsim/v1"
)

// NB: caller must Preload any relations; elaborate parameter only controls
// output of them, to give caller more flexibility.

func (in *Annotation) ToProto(out *dst_v1.Annotation, elaborate bool) error {
	out.Id = in.Id.String()
	out.ObservationId = in.ObservationId.String()
	out.Type = in.Type
	out.Name = in.Name
	out.Description = in.Description
	out.Value = in.Value.String()
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}

	return nil
}

func (in *Observation) ToProto(out *dst_v1.Observation, elaborate bool) error {
	out.Id = in.Id.String()
	out.MonitorId = in.MonitorId.String()
	if in.CollectionId != nil {
		s := in.CollectionId.String()
		out.CollectionId = &s
	}
	out.Types = in.Types
	out.Format = in.Format
	out.Description = in.Description
	out.MinFreq = in.MinFreq
	out.MaxFreq = in.MaxFreq
	out.FreqStep = in.FreqStep
	out.StartsAt = timestamppb.New(in.StartsAt)
	if in.EndsAt != nil {
		out.EndsAt = timestamppb.New(*in.EndsAt)
	}
	out.Data = in.Data
	out.Violation = in.Violation
	if in.ViolationVerifiedAt != nil {
		out.ViolationVerifiedAt = timestamppb.New(*in.ViolationVerifiedAt)
	}
	out.Interference = in.Interference
	if in.InterferenceVerifiedAt != nil {
		out.InterferenceVerifiedAt = timestamppb.New(*in.InterferenceVerifiedAt)
	}
	out.ElementId = in.ElementId.String()
	out.CreatorId = in.CreatorId.String()
	if in.UpdaterId != nil {
		s := in.UpdaterId.String()
		out.UpdaterId = &s
	}
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.UpdatedAt != nil {
		out.UpdatedAt = timestamppb.New(*in.UpdatedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate && len(in.Annotations) > 0 {
		for _, in_ann := range in.Annotations {
			out_ann := new(dst_v1.Annotation)
			in_ann.ToProto(out_ann, elaborate)
			out.Annotations = append(out.Annotations, out_ann)
		}
	}
	if in.ViolatedGrantId != nil {
		s := in.ViolatedGrantId.String()
		out.ViolatedGrantId = &s
	}

	return nil
}

func PropsimOutputKindFromProto(x propsim_v1.MapKind) (PropsimOutputKind, error) {
	if x == propsim_v1.MapKind_MK_UNSPECIFIED {
		return UnknownKind, nil
	} else if x == propsim_v1.MapKind_MK_OTHER {
		return OtherKind, nil
	} else if x == propsim_v1.MapKind_MK_PATHLOSS {
		return Pathloss, nil
	} else if x == propsim_v1.MapKind_MK_POWER {
		return Power, nil
	} else if x == propsim_v1.MapKind_MK_LOS {
		return LineOfSight, nil
	} else {
		return UnknownKind, errors.New("unrecognized PropsimOutputKind")
	}
}

func PropsimOutputUnitFromProto(x propsim_v1.MapUnit) (PropsimOutputUnit, error) {
	if x == propsim_v1.MapUnit_MU_UNSPECIFIED {
		return UnknownUnit, nil
	} else if x == propsim_v1.MapUnit_MU_dB {
		return dB, nil
	} else if x == propsim_v1.MapUnit_MU_dBm {
		return dBm, nil
	} else if x == propsim_v1.MapUnit_MU_mW {
		return mW, nil
	} else if x == propsim_v1.MapUnit_MU_LOS {
		return LOS, nil
	} else if x == propsim_v1.MapUnit_MU_OTHER {
		return OtherUnit, nil
	} else {
		return UnknownUnit, errors.New("unrecognized PropsimOutputUnit")
	}
}

func (in *PropsimPowerGrantJobOutput) ToProto(out *dst_v1.PropsimPowerGrantJobOutput, elaborate bool) error {
	out.Id = in.Id.String()
	out.PropsimPowerGrantId = in.PropsimPowerGrantId.String()
	out.PropsimJobOutputId = in.PropsimJobOutputId.String()

	return nil
}

func (in *PropsimPowerGrant) ToProto(out *dst_v1.PropsimPowerGrant, elaborate bool) error {
	out.Id = in.Id.String()
	out.GrantId = in.GrantId.String()
	out.Size = in.Size
	out.GeoserverWorkspace = in.GeoserverWorkspace
	out.GeoserverLayer = in.GeoserverLayer
	out.CreatedAt = timestamppb.New(in.CreatedAt)
	if in.FinishedAt != nil {
		out.FinishedAt = timestamppb.New(*in.FinishedAt)
	}
	if in.DeletedAt != nil {
		out.DeletedAt = timestamppb.New(*in.DeletedAt)
	}
	if elaborate && len(in.JobOutputs) > 0 {
		for _, in_i := range in.JobOutputs {
			out_i := new(dst_v1.PropsimPowerGrantJobOutput)
			in_i.ToProto(out_i, elaborate)
			out.JobOutputs = append(out.JobOutputs, out_i)
		}
	}

	return nil
}
