// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package store

import (
	"errors"
	"time"

	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	//"github.com/google/uuid"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/config"
)

func GetDatabase(serverConfig *config.Config, dbConfig *gorm.Config) (db *gorm.DB, err error) {
	var localConfig gorm.Config
	if dbConfig != nil {
		localConfig = *dbConfig
	}
	localConfig.NowFunc = func() time.Time { return time.Now().UTC() }
	switch serverConfig.DbDriver {
	case "sqlite":
		db, err = gorm.Open(sqlite.Open(serverConfig.DbDsn), &localConfig)
	case "postgres":
		db, err = gorm.Open(postgres.Open(serverConfig.DbDsn), &localConfig)
	default:
		db, err = nil, errors.New("unsupported database driver")
	}
	return db, err
}

func InitDatabase(serverConfig *config.Config, db *gorm.DB) (err error) {
	if serverConfig.DbAutoMigrate {
		err = MigrateDatabase(serverConfig, db)
	}
	return err
}

func MigrateDatabase(serverConfig *config.Config, db *gorm.DB) error {
	// Ensure postgis stuff is enabled.
	db.Debug().Exec(`
	DO $$ BEGIN
        CREATE EXTENSION IF NOT EXISTS postgis;
        CREATE EXTENSION IF NOT EXISTS postgis_raster;
        CREATE EXTENSION IF NOT EXISTS postgis_topology;
	END $$;`)
	// NB: have to do the enumerated types manually on postgres, which is
	// all we support right now.
	if err := CreatePropsimStatusEnum(db); err != nil {
		return err
	}
	if err := CreatePropsimOutputTypeEnum(db); err != nil {
		return err
	}
	if err := CreatePropsimOutputKindEnum(db); err != nil {
		return err
	}
	if err := CreatePropsimOutputUnitEnum(db); err != nil {
		return err
	}
	if err := db.AutoMigrate(
		&Area{}, &AreaPoint{}, &Collection{}, &Observation{}, &Annotation{},
		&PropsimRaster{}, &Propsim{}, &PropsimJob{}, &PropsimJobOutput{},
		&PropsimPowerGrant{}, &PropsimPowerGrantJobOutput{},
		&PropsimPowerGlobal{}, &PropsimPowerGlobalGrant{},
	); err != nil {
		return err
	}
	if err := CreatePropsimRasterIdx(db); err != nil {
		return err
	}
	return nil
}

func Paginate(page int, itemsPerPage int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		offset := (page - 1) * itemsPerPage
		return db.Offset(offset).Limit(itemsPerPage)
	}
}

func MakeILikeClause(config *config.Config, field string) string {
	if config.DbDriver == "postgres" {
		return field + " ilike ?"
	} else {
		return field + " like ?"
	}
}
