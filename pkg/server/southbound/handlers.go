// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package southbound

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"math"
	"sort"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/peer"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
	"gorm.io/gorm"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/observations"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/sensors/scos"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/propsim/geoserver"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

func (s *Server) GetObservation(ctx context.Context, req *dst.GetObservationRequest) (resp *dst.GetObservationResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid ObservationRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var id uuid.UUID
	if id, err = uuid.Parse(req.Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid observation id")
	}
	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	log.Info().Msg(fmt.Sprintf("GetObservation(%+v)", req))

	var item store.Observation
	q := s.db.Where(&store.Observation{Id: id})
	if elaborate {
		q = q.Preload("Annotations")
	}
	res := q.First(&item)
	if res.Error != nil || res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "Observation not found")
	}

	// NB: we exclude any unapproved or deleted RoleBindings.
	var outval dst.Observation
	if err := item.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal Observation")
	}

	resp = &dst.GetObservationResponse{
		Header:      &dst.ResponseHeader{},
		Observation: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	return resp, nil
}

func (s *Server) GetObservations(ctx context.Context, req *dst.GetObservationsRequest) (resp *dst.GetObservationsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid ObservationRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	log.Info().Msg(fmt.Sprintf("GetObservations(%+v)", req))

	return nil, status.Errorf(codes.Unimplemented, "unimplemented")
}

func (s *Server) CollectSpectrumObservation(ctx context.Context, minFreq int64, maxFreq int64) (collection *store.Collection, err error) {
	var zmcClient zmc.ZmcClient
	if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	var monitor zmc.Monitor
	stype := "scos"
	if resp, err := zmcClient.GetMonitors(ctx, &zmc.GetMonitorsRequest{Types: &stype}); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain monitors from zmc service")
	} else if resp.Monitors == nil || len(resp.Monitors) == 0 {
		return nil, status.Errorf(codes.Internal, "No monitors available from zmc service")
	} else {
		monitor = *resp.Monitors[0]
	}

	var scosClient *scos.ScosClient
	if scosClient, err = scos.ScosClientFromMonitor(&monitor); err != nil {
		return nil, status.Errorf(codes.Internal, "cannot generate scos client from monitor: %+v", err)
	}
	//log.Debug().Msg(fmt.Sprintf("scos client: %+v", scosClient))

	if collection, err = scosClient.CollectSpectrumFft(minFreq, maxFreq); err != nil {
		return nil, status.Errorf(codes.Internal, "cannot collect spectrum occupancy data: %+v", err)
	}
	if res := s.db.Create(collection); res.Error != nil {
		return nil, status.Errorf(codes.Internal, "error storing scos observations: %+v", res.Error)
	}
	log.Debug().Msg(fmt.Sprintf("collection: %+v (%d, %d, %+v, %+v", collection.Id, collection.MinFreq, collection.MaxFreq, collection.StartsAt, collection.EndsAt))

	return collection, nil
}

func Int64Max(x, y int64) int64 {
	if x > y {
		return x
	} else {
		return y
	}
}

func Int64Min(x, y int64) int64 {
	if x < y {
		return x
	} else {
		return y
	}
}

// Input ranges must be presorted.
func IntersectRangeLists(old [][2]int64, new [][2]int64) (ret [][2]int64) {
	ret = make([][2]int64, 0, 0)

	// If either input list is empty, the intersection is nil.
	if old == nil || len(old) == 0 {
		return ret
	} else if new == nil || len(new) == 0 {
		return ret
	}

	oi := 0
	for _, nv := range new {
		nx := nv[0]
		ny := nv[1]
		if oi == len(old) {
			log.Debug().Msg(fmt.Sprintf("new [%d,%d] above all old; skipping remaining new", nx, ny))
			break
		}
		for oi < len(old) {
			ox := old[oi][0]
			oy := old[oi][1]
			if ny <= ox {
				// New range is below old range; skip entirely.
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] below old [%d,%d]; skipping new", nx, ny, ox, oy))
				break
			} else if nx >= oy {
				// New range is above current old range; skip old range.
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] above old [%d,%d]; skipping old", nx, ny, ox, oy))
				oi += 1
				continue
			} else {
				// Current new and and old overlap; compute intersection and
				// add that.
				mx := Int64Max(nx, ox)
				my := Int64Min(ny, oy)
				log.Debug().Msg(fmt.Sprintf("new [%d,%d] overlaps old [%d,%d]; merged [%d,%d]", nx, ny, ox, oy, mx, my))
				ret = append(ret, [2]int64{mx, my})
				// Continue -- there could be a subsequent old range that
				// also overlaps with this new range.
				oi += 1
				continue
			}
		}
	}

	return ret
}

func (s *Server) GetUnoccupiedSpectrum(ctx context.Context, req *dst.GetUnoccupiedSpectrumRequest) (resp *dst.GetUnoccupiedSpectrumResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetUnoccupiedSpectrumRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var monitorIdList []*uuid.UUID
	if req.MonitorIds != nil {
		for _, mId := range req.MonitorIds {
			if idParsed, err := uuid.Parse(mId); err != nil {
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid GetUnoccupiedSpectrumRequest: invalid monitor_ids item '%s'", mId))
			} else {
				monitorIdList = append(monitorIdList, &idParsed)
			}
		}
	}

	log.Info().Msg(fmt.Sprintf("GetUnoccupiedSpectrum(%+v)", req))

	//
	// If there are recent observations, simply use those.  Otherwise, generate some.
	//
	var collections []store.Collection
	q := s.db.Where(
		s.db.Where(store.MakeILikeClause(s.config, "Collections.types"), "%scos%").
			Or(store.MakeILikeClause(s.config, "Collections.format"), "%psd-csv-ota%")).
		Where("Collections.min_freq <= ?", req.MinFreq).
		Where("Collections.max_freq >= ?", req.MaxFreq)
	if req.Type != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Collections.types"), fmt.Sprintf("%%%s%%", *req.Type))
	}
	if req.Format != nil {
		q = q.Where(store.MakeILikeClause(s.config, "Collections.formats"), fmt.Sprintf("%%%s%%", *req.Format))
	}
	if req.Type == nil && req.Format == nil {
		q = q.Where(s.db.Where(
			s.db.Where(store.MakeILikeClause(s.config, "Collections.types"), "%scos%").
				Or(store.MakeILikeClause(s.config, "Collections.format"), "%psd-csv-ota%")))
	}
	if monitorIdList != nil && len(monitorIdList) > 0 {
		q = q.Where("Collections.monitor_id in ?", monitorIdList)
	}
	q = q.Preload("Observations", func(db *gorm.DB) *gorm.DB {
			return db.Order("observations.min_freq asc")
		}).
		Preload("Observations.Annotations", func(db *gorm.DB) *gorm.DB {
			return db.Where("Annotations.type = 'psd-mean'")
		}).
		Order("Collections.starts_at desc")
	if req.StartsAt != nil {
		q = q.Where("Collections.starts_at > ?", req.StartsAt.AsTime())
		if req.EndsAt != nil {
			q = q.Where("Collections.starts_at < ?", req.EndsAt.AsTime())
		}
	} else {
		q = q.Limit(1)
	}
	if qres := q.Find(&collections); qres.RowsAffected < 1 {
		if collection, qerr := s.CollectSpectrumObservation(ctx, req.MinFreq, req.MaxFreq); qerr != nil {
			return nil, status.Errorf(codes.Internal, "error collecting spectrum: %+v", qerr)
		} else {
			collections = append(collections, *collection)
		}
	}

	absMinFreq := int64(math.MaxInt64)
	absMaxFreq := int64(math.MinInt64)

	// Determine absolute min/max frequencies of possible Observations to
	// set up power aggregate bins.
	for _, collection := range collections {
		for _, o := range collection.Observations {
			if o.MinFreq < absMinFreq {
				absMinFreq = o.MinFreq
			}
			if o.MaxFreq > absMaxFreq {
				absMaxFreq = o.MaxFreq
			}
		}
	}

	binSizeHz := int64(1 * 1000 * 1000)
	binMinFreq := int64(math.Floor(float64(absMinFreq) / float64(binSizeHz)))
	binMaxFreq := int64(math.Ceil(float64(absMaxFreq) / float64(binSizeHz)))
	numBins := int(binMaxFreq - binMinFreq) + 1

	binPowerValue := make([]float64, numBins)
	binPowerCount := make([]int64, numBins)

	// Walk the collections and look for sub-threshold occupancy ranges
	var merged [][2]int64
	minStartsAt := time.Now().UTC()
	for _, collection := range collections {
		ranges := make([][2]int64, 0)
		var minf int64 = 0
		var maxf int64 = 0
		log.Debug().Msg(fmt.Sprintf("checking collection %+v", collection.Id))

		cBinPowerValue := make([]float64, numBins)
		cBinPowerCount := make([]int64, numBins)

		for _, o := range collection.Observations {
			log.Debug().Msg(fmt.Sprintf("checking observation %v-%v", o.MinFreq, o.MaxFreq))
			//if minf == 0 {
			//	minf = o.MinFreq
			//}
			var annValues []observations.PsdAnnotationValue
			for _, a := range o.Annotations {
				var annValue observations.PsdAnnotationValue
				if err = json.Unmarshal(a.Value, &annValue); err != nil {
					return nil, status.Errorf(codes.Internal, "bad psd-mean annotation value: %+v", err)
				}
				annValues = append(annValues, annValue)
			}
			sort.SliceStable(annValues, func(i, j int) bool {
				if annValues[i].MinFreq < annValues[j].MinFreq {
					return true
				} else {
					return false
				}
			})

			for _, annValue := range annValues {
				step := (annValue.MaxFreq - annValue.MinFreq) / int64(len(annValue.Series))

				//log.Debug().Msg(fmt.Sprintf("data: %+v", annValue.Series))

				for i := 0; i < len(annValue.Series); i += 1 {
					//log.Debug().Msg(fmt.Sprintf("CSV: %v,%v", annValue.MinFreq+int64(i)*step, annValue.Series[i]))
					vf := annValue.MinFreq + int64(i)*step
					bin := int((vf - absMinFreq) / binSizeHz)
					cBinPowerValue[bin] += annValue.Series[i]
					cBinPowerCount[bin] += 1

					if annValue.Series[i] > req.OccupancyThreshold {
						//log.Debug().Msg(fmt.Sprintf("above: %v (%v)", annValue.MinFreq+int64(i)*step, annValue.Series[i]))
						if minf != 0 {
							if req.MinBandwidth == nil || (maxf-minf) > *req.MinBandwidth {
								log.Debug().Msg(fmt.Sprintf("adding range: %v-%v", minf, maxf)) //, annValue.Series[i]))
								ranges = append(ranges, [2]int64{minf, maxf})
							} else {
								//log.Debug().Msg(fmt.Sprintf("skipping range: %v-%v", minf, maxf)) //, annValue.Series[i]))
							}
							minf = 0
							maxf = 0
						}
					} else {
						if minf == 0 {
							minf = vf
							log.Debug().Msg(fmt.Sprintf("starting range: %v (%v)", minf, annValue.Series[i]))
						}
						maxf = vf
					}
				}
			}
		}
		if minf != 0 && (req.MinBandwidth == nil || (maxf-minf) > *req.MinBandwidth) {
			ranges = append(ranges, [2]int64{minf, maxf})
			minf = 0
			maxf = 0
		}

		//log.Debug().Msg("collection power bins:")
		for bi := 0; bi < numBins; bi++ {
			if cBinPowerCount[bi] == 0 {
				continue
			}
			binPowerValue[bi] += cBinPowerValue[bi] / float64(cBinPowerCount[bi])
			binPowerCount[bi] += 1
			//log.Debug().Msg(fmt.Sprintf("%f: %f (%d)", float64(binMinFreq * binSizeHz + int64(bi) * binSizeHz) / 1000 / 1000, cBinPowerValue[bi], cBinPowerCount[bi]))
		}

		log.Debug().Msg(fmt.Sprintf("collection ranges: %+v", ranges))

		if collection.StartsAt.Before(minStartsAt) {
			minStartsAt = collection.StartsAt
		}

		if merged == nil || len(merged) == 0 {
			merged = ranges
		} else {
			merged = IntersectRangeLists(merged, ranges)
			log.Debug().Msg(fmt.Sprintf("merged: %+v", merged))

			if len(merged) == 0 {
				log.Debug().Msg(fmt.Sprintf("merged: empty, aborting"))
				break
			}
		}
	}

	//log.Debug().Msg("power bins:")
	for bi := 0; bi < numBins; bi++ {
		if binPowerCount[bi] > 0 {
			binPowerValue[bi] /= float64(binPowerCount[bi])
		}
		//log.Debug().Msg(fmt.Sprintf("%f: %f (%d)", float64(binMinFreq * binSizeHz + int64(bi) * binSizeHz) / 1000 / 1000, binPowerValue[bi], binPowerCount[bi]))
	}

	// Ensure that our bounds all respect MinFreq/MaxFreq/Bandwidth; the
	// query above may have processed data outside of the ranges of the
	// original request.
	if merged != nil && len(merged) > 0 {
		filtered := make([][2]int64, 0, len(merged))
		for _, m := range merged {
			if m[0] < req.MinFreq {
				m[0] = req.MinFreq
			}
			if m[1] > req.MaxFreq {
				m[1] = req.MaxFreq
			}
			d := m[1] - m[0]
			if d > 0 && (req.MinBandwidth != nil && d >= *req.MinBandwidth) {
				filtered = append(filtered, [2]int64{m[0], m[1]})
			}
		}
		log.Debug().Msg(fmt.Sprintf("filtered: %+v (%+v)", filtered, req.MinBandwidth))
		merged = filtered
	}

	// If there was a MinBandwidth specified in the request, calculate
	// prioritized least-occupied subranges based on the integral-average
	// bin power values:
	prioRanges := make([][3]float64, 0)
	if req.MinBandwidth != nil && len(merged) > 0 {
		stepSize := binSizeHz * 5
		for _, r := range merged {
			baseBinFreq := r[0] - absMinFreq
			nBins := int(*req.MinBandwidth / binSizeHz)
			for (baseBinFreq + *req.MinBandwidth) < (r[1] - absMinFreq) {
				baseBin := int(baseBinFreq / binSizeHz)
				// Sum the power in the bins involved in this range:
				bPower := float64(0)
				for bi := baseBin; bi < (baseBin + nBins); bi++ {
					bPower += binPowerValue[bi]
				}
				bPower /= float64(nBins)
				prioRanges = append(prioRanges, [3]float64{float64(baseBinFreq + absMinFreq), float64(baseBinFreq + absMinFreq + *req.MinBandwidth), bPower})
				baseBinFreq += stepSize
			}
		}
		if len(prioRanges) > 0 {
			sort.SliceStable(prioRanges, func(i, j int) bool {
				if prioRanges[i][2] < prioRanges[j][2] {
					return true
				} else {
					return false
				}
			})
		}
	}

	resp = &dst.GetUnoccupiedSpectrumResponse{
		Header:   &dst.ResponseHeader{},
		StartsAt: timestamppb.New(minStartsAt),
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}
	for _, r := range prioRanges {
		log.Debug().Msg(fmt.Sprintf("prio range: %f-%f (%f)", r[0] / 1.0e6, r[1] / 1.0e6, r[2]))
		resp.Ranges = append(resp.Ranges, &dst.FrequencyRange{MinFreq: int64(r[0]), MaxFreq: int64(r[1])})
	}
	for _, r := range merged {
		log.Debug().Msg(fmt.Sprintf("full range: %f-%f", float64(r[0]) / 1.0e6, float64(r[1]) / 1.0e6))
		resp.Ranges = append(resp.Ranges, &dst.FrequencyRange{MinFreq: r[0], MaxFreq: r[1]})
	}

	return resp, nil
}

func (s *Server) GetBootstrapZoneLineString(ctx context.Context) (*string, error) {
	z, err := s.rclient.GetBootstrapZone(ctx, true)
	if err != nil {
		return nil, err
	}
	if z.Area == nil || len(z.Area.Points) == 0 {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid bootstrap zone without area points")
	}
	zls := "LINESTRING("
	var x0, y0, xN, yN float64
	for i, p := range z.Area.Points {
		if i == 0 {
			x0 = p.X
			y0 = p.Y
		} else {
			zls = zls + ", "
			xN = p.X
			yN = p.Y
		}
		zls = zls + fmt.Sprintf("%f %f", p.X, p.Y)
	}
	if x0 != xN || y0 != yN {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid bootstrap zone: first coordinate does not match final coordinate")
	}
	zls = zls + ")"
	return &zls, nil
}

func (s *Server) GetTxConstraints(ctx context.Context, req *dst.GetTxConstraintsRequest) (resp *dst.GetTxConstraintsResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetTxConstraintsRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var radioPortId uuid.UUID
	if radioPortId, err = uuid.Parse(req.RadioPortId); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid radio_port_id")
	}
	var zoneLineString *string
	if zoneLineString, err = s.GetBootstrapZoneLineString(ctx); err != nil {
		return nil, err
	}

	log.Info().Msg(fmt.Sprintf("GetTxConstraints(%+v)", req))

	// Allow some slop in the simulation vs target frequency
	minFreq := req.Freq - 100*1000000
	maxFreq := req.Freq + 100*1000000

	var propsimJobOutput store.PropsimJobOutput
	var propsim store.Propsim
	//q := s.db.Where("propsims.radio_port_id=?", radioPortId).
	q := s.db.
		Joins("left join propsim_jobs on propsim_jobs.id=propsim_job_outputs.propsim_job_id").
		Joins("left join propsims on propsims.id=propsim_jobs.propsim_id").
		Where("propsims.radio_port_id=?", radioPortId).
		Where("propsims.deleted_at is NULL").
		Where("propsims.status = ?", store.Complete).
		Where("propsims.freq >= ?", minFreq).
		Where("propsims.freq <= ?", maxFreq).
		Where("propsim_job_outputs.propsim_raster_rid is not NULL").
		Order("propsims.finished_at desc")
	if qres := q.First(&propsimJobOutput, &propsim); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "no matching simulation: %+v", err)
	}
	type Result struct {
		Max float64
	}
	q = s.db.Raw("select (ST_SummaryStats(ST_Clip(psr.rast,ST_Difference(ST_ConvexHull(psr.rast), ST_ConvexHull(ST_Polygon(?::geometry, 4326)))::geometry))).max as max from propsim_rasters as psr where psr.rid = ?", *zoneLineString, propsimJobOutput.PropsimRasterRid)
	var res Result
	if qres := q.First(&res); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.Internal, "no matching raster for simulation %d", propsimJobOutput.PropsimRasterRid)
	}

	resp = &dst.GetTxConstraintsResponse{
		Header:   &dst.ResponseHeader{},
		MaxPower: res.Max,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("GetTxConstraints(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}

func (s *Server) GetExpectedLoss(ctx context.Context, req *dst.GetExpectedLossRequest) (resp *dst.GetExpectedLossResponse, err error) {
	if req == nil || req.Point == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetExpectedLossRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var radioPortId uuid.UUID
	if radioPortId, err = uuid.Parse(req.RadioPortId); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid radio_port_id")
	}

	log.Info().Msg(fmt.Sprintf("GetExpectedLoss(%+v)", req))

	minFreq := req.Freq
	maxFreq := req.Freq
	if req.Bandwidth != nil {
		minFreq = minFreq - (*req.Bandwidth / 2)
		maxFreq = maxFreq + (*req.Bandwidth / 2)
	}

	var propsimJobOutput store.PropsimJobOutput
	var propsim store.Propsim
	q := s.db.
		Joins("left join propsim_jobs on propsim_jobs.id=propsim_job_outputs.propsim_job_id").
		Joins("left join propsims on propsims.id=propsim_jobs.propsim_id").
		Where("propsims.radio_port_id=?", radioPortId).
		Where("propsims.deleted_at is NULL").
		Where("propsims.status = ?", store.Complete).
		Where("propsims.freq >= ?", minFreq).
		Where("propsims.freq <= ?", maxFreq).
		Where("(propsims.gain is NULL or propsims.gain = 0)").
		Where("propsim_job_outputs.propsim_raster_rid is not NULL").
		Order("propsims.finished_at desc")
	if qres := q.First(&propsimJobOutput, &propsim); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, "no matching simulation: %+v", err)
	}
	type Result struct {
		Value float64
	}
	q = s.db.Raw("select ST_Value(propsim_rasters.rast, ST_Point(?, ?, ?)::geometry) as value from propsim_rasters where ST_Intersects(propsim_rasters.rast, ST_Point(?, ?, ?)::geometry) and propsim_rasters.rid = ?", req.Point.X, req.Point.Y, req.Point.Srid, req.Point.X, req.Point.Y, req.Point.Srid, propsimJobOutput.PropsimRasterRid)
	var res Result
	if qres := q.First(&res); qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.Internal, "no matching raster for simulation %d", propsimJobOutput.PropsimRasterRid)
	}

	resp = &dst.GetExpectedLossResponse{
		Header:   &dst.ResponseHeader{},
		Loss: res.Value,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("GetExpectedLoss(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}

func (s *Server) GetExpectedPower(ctx context.Context, req *dst.GetExpectedPowerRequest) (resp *dst.GetExpectedPowerResponse, err error) {
	if req == nil || req.Point == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid GetExpectedPowerRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}

	// If the request has no filtering parameters set, default Time to now.
	if req.Time == nil && req.GrantId == nil && req.RadioPortId == nil {
		t := time.Now().UTC()
		req.Time = timestamppb.New(t)
	}

	// Assemble a list of RadioPorts for which to query the path loss
	// simulations and compute expected power.  Cases:
	//
	// * If time is set: collect all grants live at that time, and analyze
	//   all their ports according to operating constraints.
	//   - If grant_id or radio_port_id are set: error
	//
	// * If grant_id is set: collect that grant and analyze its ports
	//   according to operating characteristics.
	//   - If radio_port_id is set, only analyze that port within the grant;
	//     if port is not in grant, error.
	//
	// * If radio_port_id is set alone, return expected power based on the
	//   radio port's max tx power.
	//   - time is set: error

	// Initially, this list holds per-port transmit power.  We add the path
	// loss from each port after checking the path loss simulation, and
	// compute the max power after querying.
	portMap := make(map[uuid.UUID]*dst.PowerPerRadioPort)
	portRasterMap := make(map[uuid.UUID]int)

	if req.Time != nil {
		if req.RadioPortId != nil || req.GrantId != nil {
			msg := "Cannot specify time with either of radio_port_id or grant_id"
			return nil, status.Errorf(codes.InvalidArgument, msg)
		}

		// Get grants at this time:
		var zmcClient zmc.ZmcClient
		if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
			return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
		}
		reqId := uuid.New().String()
		lt := true
		lf := false
		hdr := zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &lt,
		}
		req := zmc.GetGrantsRequest{
			Header:   &hdr,
			Time:     req.Time,
			Freq:     &req.Freq,
			Approved: &lt,
			Revoked:  &lf,
			Deleted:  &lf,
		}
		var gresp *zmc.GetGrantsResponse
		var gerr error
		if gresp, gerr = zmcClient.GetGrants(ctx, &req); gerr != nil {
			return nil, status.Errorf(codes.Internal, "Failed to obtain grants from zmc service")
		} else if gresp.Grants == nil || len(gresp.Grants) == 0 {
			return nil, status.Errorf(codes.NotFound, "No grants available from zmc service")
		}
		for _, g := range gresp.Grants {
			var cp *float64
			if len(g.Constraints) > 0 {
				cp = &g.Constraints[0].MaxEirp
			}
			for _, rp := range g.RadioPorts {
				pp := cp
				if pp == nil {
					pp = &rp.MaxPower
				}
				var rpid uuid.UUID
				if rpid, err = uuid.Parse(rp.Id); err != nil {
					return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid radio_port.id %s", rp.Id))
				}
				portMap[rpid] = &dst.PowerPerRadioPort{
					RadioPortId: rp.Id,
					GrantId: &g.Id,
					Power: *pp,
				}
			}
		}
	} else if req.GrantId != nil {
		if req.Time != nil || req.RadioPortId != nil {
			msg := "Cannot specify grant_id with either of time or radio_port_id set"
			return nil, status.Errorf(codes.InvalidArgument, msg)
		}

		// Get grant:
		var zmcClient zmc.ZmcClient
		if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
			return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
		}
		reqId := uuid.New().String()
		lt := true
		hdr := zmc.RequestHeader{
			ReqId:     &reqId,
			Elaborate: &lt,
		}
		req := zmc.GetGrantRequest{
			Header:   &hdr,
			Id:       *req.GrantId,
		}
		var gresp *zmc.GetGrantResponse
		var gerr error
		if gresp, gerr = zmcClient.GetGrant(ctx, &req); gerr != nil {
			return nil, status.Errorf(codes.Internal, "Failed to obtain grant from zmc service")
		} else if gresp.Grant == nil {
			return nil, status.Errorf(codes.NotFound, "No grant available from zmc service")
		}
		g := gresp.Grant
		var cp *float64
		if len(g.Constraints) > 0 {
			cp = &g.Constraints[0].MaxEirp
		}
		for _, rp := range g.RadioPorts {
			pp := cp
			if pp == nil {
				pp = &rp.MaxPower
			}
			var rpid uuid.UUID
			if rpid, err = uuid.Parse(rp.Id); err != nil {
				return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid radio_port.id %s", rp.Id))
			}
			portMap[rpid] = &dst.PowerPerRadioPort{
				RadioPortId: rp.Id,
				GrantId: &g.Id,
				Power: *pp,
			}
		}
	} else if req.RadioPortId != nil {
		// Get the RadioPort from the zmc:
		var radioPortId uuid.UUID
		if radioPortId, err = uuid.Parse(*req.RadioPortId); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Invalid radio_port_id")
		}
		var rp *zmc.RadioPort
		if rp, err = s.rclient.GetRadioPort(ctx, &radioPortId, true); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, "Failed to obtain radio port from zmc service")
		}
		portMap[radioPortId] = &dst.PowerPerRadioPort{
			RadioPortId: rp.Id,
			GrantId: nil,
			Power: rp.MaxPower,
		}
	} else {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid request")
	}

	log.Info().Msg(fmt.Sprintf("GetExpectedPower(%+v)", req))

	minFreq := req.Freq
	maxFreq := req.Freq
	if req.Bandwidth != nil {
		minFreq = minFreq - (*req.Bandwidth / 2)
		maxFreq = maxFreq + (*req.Bandwidth / 2)
	}

	// Verify that there is a simulation for each port to check, before
	// reading any rasters.
	for portUuid, _ := range portMap {
		var propsimJobOutput store.PropsimJobOutput
		var propsim store.Propsim
		q := s.db.
			Joins("left join propsim_jobs on propsim_jobs.id=propsim_job_outputs.propsim_job_id").
			Joins("left join propsims on propsims.id=propsim_jobs.propsim_id").
			Where("propsims.radio_port_id=?", portUuid).
			Where("propsims.deleted_at is NULL").
			Where("propsims.status = ?", store.Complete).
			Where("propsims.freq >= ?", minFreq).
			Where("propsims.freq <= ?", maxFreq).
			Where("(propsims.gain is NULL or propsims.gain = 0)").
			Where("propsim_job_outputs.propsim_raster_rid is not NULL").
			Order("propsims.finished_at desc")
		if qres := q.First(&propsimJobOutput, &propsim); qres.RowsAffected != 1 {
			return nil, status.Errorf(codes.NotFound, "no matching simulation for radio_port_id %s: %+v", portUuid, err)
		}
		portRasterMap[portUuid] = *propsimJobOutput.PropsimRasterRid
	}

	// Compute pathloss from each transmitter to each received location.
	maxPower := -math.MaxFloat64
	var maxPowerPortUuid string
	for portUuid, port := range portMap {
		propsimRasterRid, _ := portRasterMap[portUuid]
		type Result struct {
			Value float64
		}
		q := s.db.Raw("select ST_Value(propsim_rasters.rast, ST_Point(?, ?, ?)::geometry) as value from propsim_rasters where ST_Intersects(propsim_rasters.rast, ST_Point(?, ?, ?)::geometry) and propsim_rasters.rid = ?", req.Point.X, req.Point.Y, req.Point.Srid, req.Point.X, req.Point.Y, req.Point.Srid, propsimRasterRid)
		var res Result
		if qres := q.First(&res); qres.RowsAffected != 1 {
			return nil, status.Errorf(codes.Internal, "no matching raster for simulation %d", propsimRasterRid)
		}

		port.Power = port.Power + res.Value
		if port.Power > maxPower {
			maxPower = port.Power
			maxPowerPortUuid = portUuid.String()
		}
	}

	powerPerRadioPort := make([]*dst.PowerPerRadioPort, 0, len(portMap))
	for _, p := range portMap {
		powerPerRadioPort = append(powerPerRadioPort, p)
	}
	sort.Slice(powerPerRadioPort, func(i, j int) bool {
		return powerPerRadioPort[i].Power > powerPerRadioPort[i].Power
	})

	resp = &dst.GetExpectedPowerResponse{
		Header:       &dst.ResponseHeader{},
		Power:        maxPower,
		RadioPortId:  &maxPowerPortUuid,
		PowerPerRadioPort: powerPerRadioPort,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("GetExpectedPower(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}

func (s *Server) CreatePropsimPowerGrant(ctx context.Context, req *dst.CreatePropsimPowerGrantRequest) (resp *dst.CreatePropsimPowerGrantResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid CreatePropsimPowerGrantRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var grantId uuid.UUID
	if _grantId, err := uuid.Parse(req.GrantId); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid grant_id"))
	} else {
		grantId = _grantId
	}

	// Check for existence
	propsimPowerGrant := store.PropsimPowerGrant{}
	if res := s.db.Where(&store.PropsimPowerGrant{GrantId: grantId}).First(&propsimPowerGrant); res.RowsAffected == 1 {
		return nil, status.Errorf(codes.AlreadyExists, "PropsimPowerGrant exists")
	}

	elaborate := req.Header != nil && req.Header.Elaborate != nil && *req.Header.Elaborate

	// Get grant:
	var zmcClient zmc.ZmcClient
	if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	lt := true
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &lt,
	}
	greq := zmc.GetGrantRequest{
		Header:   &hdr,
		Id:       req.GrantId,
	}
	var gresp *zmc.GetGrantResponse
	var gerr error
	if gresp, gerr = zmcClient.GetGrant(ctx, &greq); gerr != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain grant from zmc service")
	} else if gresp.Grant == nil {
		return nil, status.Errorf(codes.NotFound, "No grant available from zmc service")
	}
	g := gresp.Grant
	cp := float64(0)
	minFreq := float64(0)
	maxFreq := float64(0)
	if len(g.Constraints) > 0 {
		cp = g.Constraints[0].MaxEirp
		minFreq = float64(g.Constraints[0].MinFreq - 100*1000000)
		maxFreq = float64(g.Constraints[0].MaxFreq + 100*1000000)
	} else {
		return nil, status.Errorf(codes.InvalidArgument, "No grant constraints")
	}
	type pResult struct {
		Gain *float64
		PropsimJobOutputId uuid.UUID
		PropsimRasterRid *int
	}
	// The simulation we found for this port.
	portJobOutputMap := make(map[uuid.UUID]*pResult)
	// The power delta (converted to mW) between the simulation power and
	// the constraint power; we will add this to the power-mW simulation for
	// each pixel before we aggregate.
	portMapPowerDelta := make(map[uuid.UUID]float64)
	for _, rp := range g.RadioPorts {
		var rpid uuid.UUID
		if rpid, err = uuid.Parse(rp.Id); err != nil {
			return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid radio_port.id %s", rp.Id))
		}

		// Make sure we have power-dBm simulations for this port.
		pRes := pResult{}
		q := s.db.Model(store.PropsimJobOutput{}).Select("propsims.gain, propsim_job_outputs.id as propsim_job_output_id, propsim_job_outputs.propsim_raster_rid").
			Joins("left join propsim_jobs on propsim_jobs.id=propsim_job_outputs.propsim_job_id").
			Joins("left join propsims on propsims.id=propsim_jobs.propsim_id").
			Where("propsims.radio_port_id = ?", rpid).
			Where("propsims.deleted_at is NULL").
			Where("propsims.status = ?", store.Complete).
			Where("propsims.freq >= ?", minFreq).
			Where("propsims.freq <= ?", maxFreq).
			Where("propsims.gain is not NULL").
			Where("propsim_job_outputs.propsim_raster_rid is not NULL").
			Where("propsim_job_outputs.kind = 'power'").
			Where("propsim_job_outputs.unit = 'dBm'").
			Order("propsims.finished_at desc")
		if qres := q.First(&pRes); qres.RowsAffected != 1 {
			return nil, status.Errorf(codes.NotFound, "no matching simulation for radio port %v: %+v", rpid, err)
		}
		log.Debug().Msg(fmt.Sprintf("pRes: %+v", pRes))
		portJobOutputMap[rpid] = &pRes

		// If the dBm delta is 0, we can just use the power raster as-is.
		portMapPowerDelta[rpid] = float64(*pRes.Gain) - cp
	}

	propsimPowerGrant = store.PropsimPowerGrant{
		Id:  uuid.New(),
		GrantId: grantId,
	}
	if res := s.db.Create(&propsimPowerGrant); res.Error != nil {
		return nil, status.Errorf(codes.Internal, "error storing PropsimPowerGrant: %+v", res.Error)
	}
	log.Debug().Msg(fmt.Sprintf("PropsimPowerGrant: %+v", &propsimPowerGrant))

	// Ok, now we can aggregate.  There are three basic cases:
	// * only one port, and sim power matches grant power: re-use power map raster exactly.
	// * only one port, and sim power does not match grant power
	// * multiple ports
	//
	// But for now we treat them the same way.  For each port, add delta to
	// sim via st_algebra, st_union all of those, then st_algebra outside to
	// convert back to dbm, and insert that raster!

	// select ST_MapAlgebra(ST_Union(ST_MapAlgebra(psr.rast, 1, NULL, rmt.rexpr)), 1, NULL, '10*log([rast])') from propsim_rasters as psr join (values (53, 'pow(10.0,[rast]/10.0)'),(57, 'pow(10.0,[rast]/10.0)')) as rmt(rid, rexpr) on rmt.rid=psr.propsim_raster_id;

	// with r1 as (select rast from propsim_rasters where rid=53), r2 as (select rast from propsim_rasters where rid=57) select ST_MapAlgebra(r1.rast, 1, r2.rast, 1, '10*log(pow(10.0,([rast1]-8.5)/10.0)+pow(10.0,([rast2])/10.0))', NULL, 'INTERSECTION') from r1, r2;
	
	rasterId := -1
	var incError error
	for rpid, pRes := range portJobOutputMap {
		pd := portMapPowerDelta[rpid]
		if rasterId == -1 {
			// insert into propsim_rasters (rast) select rast from propsim_rasters where rid = 57 returning rid;
			var rexpr string
			if pd == 0.0 {
				rexpr = "rast"
			} else if pd > 0 {
				rexpr = fmt.Sprintf("ST_MapAlgebra(rast, 1, NULL, '[rast]+%64.64f')", pd)
			} else {
				rexpr = fmt.Sprintf("ST_MapAlgebra(rast, 1, NULL, '[rast]%64.64f')", pd)
			}
			queryString := fmt.Sprintf("insert into propsim_rasters (rast) select %s from propsim_rasters where rid = ? returning rid", rexpr)
			res := s.db.Raw(queryString, *pRes.PropsimRasterRid).Scan(&rasterId)
			if res.Error != nil || res.RowsAffected != 1 {
				msg := fmt.Sprintf("CreatePropsimPowerGrant: failed to insert initial raster: %v", res.Error)
				log.Error().Err(res.Error).Msg(msg)
				incError = res.Error
				break
			}
		} else {
			// with r1 as (select rast from propsim_rasters where rid=53), r2 as (select rast from propsim_rasters where rid=64) update propsim_rasters set rast=(select ST_MapAlgebra(r1.rast, 1, r2.rast, 1, '10*log(pow(10.0,([rast1]-8.5)/10.0)+pow(10.0,([rast2])/10.0))', NULL, 'INTERSECTION') from r1, r2) where rid = 64;
			var rsexpr string
			if pd == 0.0 {
				rsexpr = ""
			} else if pd > 0 {
				rsexpr = fmt.Sprintf("+%f", pd)
			} else {
				rsexpr = fmt.Sprintf("%f", pd)
			}
			queryString := fmt.Sprintf("with r1 as (select rast from propsim_rasters where rid = ?), r2 as (select rast from propsim_rasters where rid = ?) update propsim_rasters set rast=(select ST_MapAlgebra(r1.rast, 1, r2.rast, 1, '10*log(pow(10.0,([rast1]%s)/10.0)+pow(10.0,([rast2])/10.0))', NULL, 'INTERSECTION') from r1, r2) where rid = ?", rsexpr)
			res := s.db.Raw(queryString, *pRes.PropsimRasterRid, rasterId, rasterId).Scan(&rasterId)
			if res.Error != nil { //|| res.RowsAffected != 1 {
				msg := fmt.Sprintf("CreatePropsimPowerGrant: failed to update raster: %v", res.Error)
				log.Error().Err(res.Error).Msg(msg)
				incError = res.Error
				break
			}
		}
	}
	if incError != nil {
		return nil, status.Errorf(codes.Internal, fmt.Sprintf("failed to create PropsimPowerGrant for grant %v", req.GrantId))
	}

	propsimPowerGrant.RasterRid = &rasterId

	// Save off the PropsimJobOutput maps we used to aggregate.
	for _, pRes := range portJobOutputMap {
		x := store.PropsimPowerGrantJobOutput{
			Id:                  uuid.New(),
			PropsimPowerGrantId: propsimPowerGrant.Id,
			PropsimJobOutputId:  pRes.PropsimJobOutputId,
		}
		s.db.Create(&x)
	}

	s.db.Save(&propsimPowerGrant)

	// Push the aggregate map to geoserver:
	if s.config.GeoserverApi != "" && s.config.GeoserverUser != "" && s.config.GeoserverPassword != "" {
		// Grab the raster for geoserver
		var data string
		res := s.db.Raw("select ST_AsTIFF(rast)::bytea as data from propsim_rasters where rid = ?", rasterId).Scan(&data)
		if res.Error != nil || res.RowsAffected != 1 {
			msg := fmt.Sprintf("CreatePropsimPowerGrant: failed to retrieve aggregate initial raster: %v", res.Error)
			log.Error().Err(res.Error).Msg(msg)
			return nil, status.Errorf(codes.Internal, fmt.Sprintf("failed to create PropsimPowerGrant for grant %v", req.GrantId))
		}
		gc := geoserver.NewGeoserverClient(s.config.GeoserverApi, s.config.GeoserverUser, s.config.GeoserverPassword, true)
		layerName := "id_ppg_" + strings.ReplaceAll(propsimPowerGrant.Id.String(), "-", "_")
		if _, _, err := gc.Import(s.config.GeoserverWorkspace, bytes.NewReader([]byte(data)), layerName, s.config.GeoserverDefaultStyle, s.config.GeoserverSrsTransform, s.config.GeoserverAnonymous); err != nil {
			msg := fmt.Sprintf("PropsimPowerGrant: failed to insert geotiff raster into geoserver (%s): %v", propsimPowerGrant.Id, err)
			log.Error().Err(err).Msg(msg)
			return nil, status.Errorf(codes.Internal, "Failed to insert PropsimPowerGrant geotiff raster into geoserver")
		} else {
			propsimPowerGrant.GeoserverWorkspace = &s.config.GeoserverWorkspace
			propsimPowerGrant.GeoserverLayer = &layerName
		}
	}

	// Save post-geoserver upload.
	t := time.Now().UTC()
	propsimPowerGrant.FinishedAt = &t
	s.db.Save(&propsimPowerGrant)

	// Refresh if elaborating to pick up outputs.
	if elaborate {
		if res := s.db.Where(&store.PropsimPowerGrant{Id: propsimPowerGrant.Id}).First(&propsimPowerGrant); res.Error != nil || res.RowsAffected != 1 {
			return nil, status.Errorf(codes.Internal, "failed to load PropsimPowerGrant after creation")
		}
	}

	outval := dst.PropsimPowerGrant{}
	if err := propsimPowerGrant.ToProto(&outval, elaborate); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to marshal PropsimPowerGrant")
	}
	resp = &dst.CreatePropsimPowerGrantResponse{
		Header:            &dst.ResponseHeader{},
		PropsimPowerGrant: &outval,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("CreatePropsimPowerGrant(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}

func (s *Server) CheckGrantConflict(ctx context.Context, req *dst.CheckGrantConflictRequest) (resp *dst.CheckGrantConflictResponse, err error) {
	if req == nil {
		return nil, status.Errorf(codes.InvalidArgument, "Invalid CheckGrantConflictRequest")
	}
	if err := req.Validate(); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, err.Error())
	}
	var grant1Id uuid.UUID
	if _grant1Id, err := uuid.Parse(req.Grant1Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid grant1_id"))
	} else {
		grant1Id = _grant1Id
	}
	var grant2Id uuid.UUID
	if _grant2Id, err := uuid.Parse(req.Grant2Id); err != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("Invalid grant2_id"))
	} else {
		grant2Id = _grant2Id
	}

	// Check for existence
	propsimPowerGrant1 := store.PropsimPowerGrant{}
	if res := s.db.Where(&store.PropsimPowerGrant{GrantId: grant1Id}).First(&propsimPowerGrant1); res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("PropsimPowerGrant not found for grant %v", grant1Id))
	}
	propsimPowerGrant2 := store.PropsimPowerGrant{}
	if res := s.db.Where(&store.PropsimPowerGrant{GrantId: grant2Id}).First(&propsimPowerGrant2); res.RowsAffected != 1 {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("PropsimPowerGrant not found for grant %v", grant2Id))
	}
	if propsimPowerGrant1.RasterRid == nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("PropsimPowerGrant for grant %v has no raster, cannot check conflict", grant1Id))
	}
	if propsimPowerGrant2.RasterRid == nil {
		return nil, status.Errorf(codes.NotFound, fmt.Sprintf("PropsimPowerGrant for grant %v has no raster, cannot check conflict", grant2Id))
	}

	// Get grant1:
	var zmcClient zmc.ZmcClient
	if zmcClient, err = s.rclient.GetZmcClient(ctx); err != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain zmc service client")
	}
	reqId := uuid.New().String()
	lt := true
	hdr := zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &lt,
	}
	greq := zmc.GetGrantRequest{
		Header:   &hdr,
		Id:       req.Grant1Id,
	}
	var g1 *zmc.Grant
	if gresp, gerr := zmcClient.GetGrant(ctx, &greq); gerr != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain grant from zmc service")
	} else if gresp.Grant == nil {
		return nil, status.Errorf(codes.NotFound, "No grant available from zmc service")
	} else {
		g1 = gresp.Grant
	}
	// Get grant2:
	reqId = uuid.New().String()
	hdr = zmc.RequestHeader{
		ReqId:     &reqId,
		Elaborate: &lt,
	}
	greq = zmc.GetGrantRequest{
		Header:   &hdr,
		Id:       req.Grant2Id,
	}
	var g2 *zmc.Grant
	if gresp, gerr := zmcClient.GetGrant(ctx, &greq); gerr != nil {
		return nil, status.Errorf(codes.Internal, "Failed to obtain grant from zmc service")
	} else if gresp.Grant == nil {
		return nil, status.Errorf(codes.NotFound, "No grant available from zmc service")
	} else {
		g2 = gresp.Grant
	}

	if len(g1.Constraints) <= 0 {
		return nil, status.Errorf(codes.InvalidArgument, "grant1 has no constraints")
	} else if len(g2.Constraints) <= 0 {
		return nil, status.Errorf(codes.InvalidArgument, "grant2 has no constraints")
	} else if len(g1.IntConstraints) <= 0 {
		return nil, status.Errorf(codes.InvalidArgument, "grant1 has no int_constraints")
	} else if len(g2.IntConstraints) <= 0 {
		return nil, status.Errorf(codes.InvalidArgument, "grant2 has no int_constraints")
	}

	g1ic := g1.IntConstraints[0].MaxPower
	g2ic := g2.IntConstraints[0].MaxPower
	if g1ic == nil {
		return nil, status.Errorf(codes.InvalidArgument, "grant1 int_constraint has no max_power")
	} else if g2ic == nil {
		return nil, status.Errorf(codes.InvalidArgument, "grant2 int_constraint has no max_power")
	}
	
	g1MinFreq := g1.Constraints[0].MinFreq
	g1MaxFreq := g1.Constraints[0].MaxFreq
	g2MinFreq := g2.Constraints[0].MinFreq
	g2MaxFreq := g2.Constraints[0].MaxFreq

	// If there is no frequency overlap, there is no conflict.
	if g1MinFreq > g2MaxFreq || g1MaxFreq < g2MinFreq {
		resp = &dst.CheckGrantConflictResponse{
			Header:     &dst.ResponseHeader{},
			IsConflict: false,
		}
		if req.Header != nil {
			resp.Header.ReqId = req.Header.ReqId
		}
		log.Info().Msg(fmt.Sprintf("CheckGrantConflict(%+v): resp %+v", req.Header.ReqId, resp))
		return resp, nil
	}

	// Intersect the power aggregate maps.  We can simply use the lowest
	// power threshold constraint; it is the most restrictive (most likely
	// for interference).
	//
	// Check if expected power for two grants overlaps according to their
    // IntConstraints.  For instance:
    // Check if A's B-interfering power (-125) enters B's de facto coverage zone
    //   if A(-125) intersects B(-125)
    // Check if B's A-intefering power (-120) enters A's de facto coverage zone
    //   if B(-120) intersects A(-120)
    // NB: we use a de facto coverage zone for now.
	gic := math.Min(*g1ic, *g2ic)
	
	type Result struct {
		StIntersects bool
	}
	var intRes Result
	queryString := fmt.Sprintf("with r1 as (select rast from propsim_rasters where rid = ?), r2 as (select rast from propsim_rasters where rid = ?) select ST_Intersects(ST_MinConvexHull(st_reclass(r1.rast, 1, '[-9999-%f):0,[%f-9999:1', '1BB', 0)),ST_MinConvexHull(st_reclass(r2.rast, 1, '[-9999-%f):0,[%f-9999:1', '1BB', 0))) from r1, r2;", gic, gic, gic, gic)
	q := s.db.Raw(queryString, *propsimPowerGrant1.RasterRid, *propsimPowerGrant2.RasterRid)
	if qres := q.First(&intRes); qres.Error != nil {
		return nil, status.Errorf(codes.InvalidArgument, fmt.Sprintf("unexpected error comparing PropsimPowerGrants for intersection: %+v", qres.Error))
	} else if qres.RowsAffected != 1 {
		return nil, status.Errorf(codes.InvalidArgument, "unexpected error comparing PropsimPowerGrants for intersection: no data")
	}

	// Send the response.
	resp = &dst.CheckGrantConflictResponse{
		Header:     &dst.ResponseHeader{},
		IsConflict: intRes.StIntersects,
	}
	if req.Header != nil {
		resp.Header.ReqId = req.Header.ReqId
	}

	log.Info().Msg(fmt.Sprintf("CheckGrantConflict(%+v): resp %+v", req.Header.ReqId, resp))

	return resp, nil
}
	
func EventToProto(e *subscription.Event, include bool, elaborate bool) (*dst.Event, error) {
	ep := &dst.Event{
		Header: e.Header.ToProto(),
	}

	if include && e.Object != nil {
		switch v := e.Object.(type) {
		case *store.Observation:
			x := new(dst.Observation)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &dst.Event_Observation{Observation: x}
		case *store.Annotation:
			x := new(dst.Annotation)
			if err := v.ToProto(x, elaborate); err != nil {
				return nil, fmt.Errorf("failed to encode object type %T: %s", v, err.Error())
			}
			ep.Object = &dst.Event_Annotation{Annotation: x}
		default:
			return nil, fmt.Errorf("unknown event object type %T", v)
		}
	}

	log.Debug().Msg(fmt.Sprintf("EventToProto: %+v", ep))

	return ep, nil
}

func (s *Server) Subscribe(req *dst.SubscribeRequest, stream dst.Dst_SubscribeServer) error {
	if req == nil || req.Header == nil || req.Header.ReqId == nil {
		return status.Errorf(codes.InvalidArgument, "Invalid SubscribeRequest")
	}
	if err := req.Validate(); err != nil {
		return status.Errorf(codes.InvalidArgument, err.Error())
	}

	var filters []subscription.EventFilter
	if req.Filters != nil && len(req.Filters) > 0 {
		filters = make([]subscription.EventFilter, 0, len(req.Filters))
		for _, f := range req.Filters {
			filters = append(filters, subscription.EventFilterFromProto(f))
		}
	}
	ch := make(chan *subscription.Event)
	sid := *req.Header.ReqId
	endpoint := ""
	if p, ok := peer.FromContext(stream.Context()); ok {
		endpoint = p.Addr.String()
	}
	if err := s.sm.Subscribe(sid, filters, ch, endpoint, subscription.Rpc, nil); err != nil {
		return status.Errorf(codes.Internal, err.Error())
	}

	// Handle write panics as well as the regular return cases.
	defer func() {
		if true || recover() != nil {
			s.sm.Unsubscribe(sid)
		}
	}()

	include := req.Include == nil || *req.Include
	elaborate := req.Header.Elaborate == nil || *req.Header.Elaborate

	// Watch for events and client connection error conditions
	// (e.g. disconnect).
	for {
		select {
		case e := <-ch:
			if ep, err := EventToProto(e, include, elaborate); err != nil {
				log.Error().Err(err).Msg("failed to marshal event to RPC")
				continue
			} else {
				events := make([]*dst.Event, 0, 1)
				events = append(events, ep)
				resp := &dst.SubscribeResponse{
					Header: &dst.ResponseHeader{
						ReqId: req.Header.ReqId,
					},
					Events: events,
				}
				if err := stream.Send(resp); err != nil {
					break
				}
			}
		case <-stream.Context().Done():
			log.Debug().Msg(fmt.Sprintf("stream closed unexpectedly (%s)", sid))
			return nil
		}
	}

	return nil
}
