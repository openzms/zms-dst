// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package models

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

// The decoded contents of a SigMF metadata file
// (https://sigmf.org/index.html#sigmf-metadata-format).
type MetadataDecoding struct {
	Metadata Metadata
	Raw      RawMetadata     `json:"-"`
	Partial  PartialMetadata `json:"-"`
}

type RawMetadata struct {
	Global      json.RawMessage   `json:"global" binding:"required"`
	Captures    []json.RawMessage `json:"captures" binding:"required"`
	Annotations []json.RawMessage `json:"annotations" binding:"required"`
}

type PartialMetadata struct {
	Global      map[string]json.RawMessage   `json:"global" binding:"required"`
	Captures    []map[string]json.RawMessage `json:"captures" binding:"required"`
	Annotations []map[string]json.RawMessage `json:"annotations" binding:"required"`
}

type Metadata struct {
	Global      Global       `json:"global" binding:"required,dive,required"`
	Captures    []Capture    `json:"captures" binding:"required,dive,required"`
	Annotations []Annotation `json:"annotations" binding:"required,dive,required"`

	// New top-level extension values; unlikely to be used.
	ExtensionValues map[string]interface{}
}

func (x *Metadata) AttachExtensionValue(namespace string, v interface{}) {
	if x.ExtensionValues == nil {
		x.ExtensionValues = make(map[string]interface{}, 1)
	}
	x.ExtensionValues[namespace] = v
}

type Global struct {
	Version       string       `json:"core:version" binding:"required"`
	Datatype      string       `json:"core:datatype" binding:"required"`
	SampleRate    *float64     `json:"core:sample_rate"`
	Author        *string      `json:"core:author"`
	Collection    *string      `json:"core:collection"`
	Dataset       *string      `json:"core:dataset"`
	DataDoi       *string      `json:"core:data_doi"`
	Description   *string      `json:"core:description"`
	Hw            *string      `json:"core:hw"`
	License       *string      `json:"core:license"`
	MetadataOnly  *bool        `json:"core:metadata_only"`
	MetaDoi       *string      `json:"core:meta_doi"`
	NumChannels   *int         `json:"core:num_channels"`
	Offset        *int         `json:"core:offset"`
	Recorder      *string      `json:"core:recorder"`
	Sha512        *string      `json:"core:sha512"`
	TrailingBytes *string      `json:"core:trailing_bytes"`
	Geolocation   *Geolocation `json:"core:geolocation"`
	Extensions    []Extension  `json:"core:extensions"`

	// Inline, per-extension Global data.
	// e.g. .ExtensionValues["ntia-core"].(*NtiaCoreGlobal).Classification
	ExtensionValues map[string]interface{}
}

func (x *Global) AttachExtensionValue(namespace string, v interface{}) {
	if x.ExtensionValues == nil {
		x.ExtensionValues = make(map[string]interface{}, 1)
	}
	x.ExtensionValues[namespace] = v
}

type Extension struct {
	Name     string `json:"name" binding:"required"`
	Version  string `json:"version" binding:"required"`
	Optional bool   `json:"optional" binding:"required"`
}

type Geolocation struct {
	Type        string    `json:"type"`
	Coordinates []float64 `json:"coordinates"`
}

type Capture struct {
	// NB: required ints must have pointer field type; see
	// https://pkg.go.dev/gopkg.in/go-playground/validator.v9#hdr-Required .
	SampleStart *int       `json:"core:sample_start" binding:"required"`
	Datetime    *time.Time `json:"core:datetime"`
	Frequency   *float64   `json:"core:frequency"`
	GlobalIndex *int       `json:"core:global_index"`
	HeaderBytes *int       `json:"core:header_bytes"`

	// Inline, per-extension, per-Capture data.
	// e.g. .ExtensionValues["ntia-sensor"].(*NtiaSensorCapture).Duration
	ExtensionValues map[string]interface{}
}

func (x *Capture) AttachExtensionValue(namespace string, v interface{}) {
	if x.ExtensionValues == nil {
		x.ExtensionValues = make(map[string]interface{}, 1)
	}
	x.ExtensionValues[namespace] = v
}

type Annotation struct {
	SampleStart   *int       `json:"core:sample_start" binding:"required"`
	SampleCount   *int       `json:"core:sample_count"`
	FreqLowerEdge *float64   `json:"core:freq_lower_edge"`
	FreqUpperEdge *float64   `json:"core:freq_upper_edge"`
	Label         *string    `json:"core:label"`
	Comment       *string    `json:"core:comment"`
	Generator     *string    `json:"core:generator"`
	Uuid          *uuid.UUID `json:"core:uuid"`

	// Inline, per-extension, per-Annotation data.
	ExtensionValues map[string]interface{}
}

func (x *Annotation) AttachExtensionValue(namespace string, v interface{}) {
	if x.ExtensionValues == nil {
		x.ExtensionValues = make(map[string]interface{}, 1)
	}
	x.ExtensionValues[namespace] = v
}
