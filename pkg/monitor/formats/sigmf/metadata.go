// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package sigmf

import (
	"encoding/json"
	"errors"
	"fmt"

	"github.com/gin-gonic/gin/binding"
	"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext/ntia"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/models"
)

// The decoded contents of a SigMF metadata file
// (https://sigmf.org/index.html#sigmf-metadata-format).
type MetadataFile struct {
	Decoding      models.MetadataDecoding
	ExtensionData map[string]*ext.ExtensionData
}

var extensionReaders map[string]ext.ExtensionReader = map[string]ext.ExtensionReader{
	"ntia-core":        ntia.NtiaCoreReader{},
	"ntia-sensor":      ntia.NtiaSensorReader{},
	"ntia-scos":        ntia.NtiaScosReader{},
	"ntia-environment": ntia.NtiaEnvironmentReader{},
	"ntia-algorithm":   ntia.NtiaAlgorithmReader{},
	"ntia-emitter":     ntia.NtiaEmitterReader{},
}

// Reads and decodes a SigMF metadata file.
func ReadMetadataFile(data []byte) (metadataFile *MetadataFile, err error) {
	// First, decode the canonical top-level fields (global, captures,
	// annotations) into json.RawMessage fields that can be used in further
	// decoding.
	var rm models.RawMetadata
	if err := json.Unmarshal(data, &rm); err != nil {
		return nil, fmt.Errorf("error processing sigmf metadata: %s", err.Error())
	} else if err := binding.Validator.ValidateStruct(&rm); err != nil {
		return nil, fmt.Errorf("error validating sigmf metadata: %s", err.Error())
	}

	// Second, decode the top-level json.RawMessage extractions into objects
	// with key-value pairs, or lists of those objects.
	var pm models.PartialMetadata
	if err := json.Unmarshal(rm.Global, &pm.Global); err != nil {
		return nil, fmt.Errorf("error processing sigmf metadata: %s", err.Error())
	} else if err := binding.Validator.ValidateStruct(&pm.Global); err != nil {
		return nil, fmt.Errorf("error validating sigmf metadata: %s", err.Error())
	}
	// For Captures and Annotations, do them by array member.
	pm.Captures = make([]map[string]json.RawMessage, len(rm.Captures))
	for i, _ := range rm.Captures {
		if err := json.Unmarshal(rm.Captures[i], &pm.Captures[i]); err != nil {
			return nil, fmt.Errorf("error processing sigmf metadata: %s", err.Error())
		} else if err := binding.Validator.ValidateStruct(&pm.Captures[i]); err != nil {
			return nil, fmt.Errorf("error validating sigmf metadata: %s", err.Error())
		}
	}
	pm.Annotations = make([]map[string]json.RawMessage, len(rm.Annotations))
	for i, _ := range rm.Annotations {
		if err := json.Unmarshal(rm.Annotations[i], &pm.Annotations[i]); err != nil {
			return nil, fmt.Errorf("error processing sigmf metadata: %s", err.Error())
		} else if err := binding.Validator.ValidateStruct(&pm.Annotations[i]); err != nil {
			return nil, fmt.Errorf("error validating sigmf metadata: %s", err.Error())
		}
	}

	// Finally, fully decode the core namespace Metadata.
	m := models.Metadata{}
	if err := json.Unmarshal(data, &m); err != nil {
		return nil, fmt.Errorf("error processing sigmf metadata: %s", err.Error())
	}
	if err := binding.Validator.ValidateStruct(&m); err != nil {
		return nil, fmt.Errorf("error validating sigmf metadata: %s", err.Error())
	}

	ret := MetadataFile{
		Decoding: models.MetadataDecoding{
			Metadata: m,
			Raw:      rm,
			Partial:  pm,
		},
		ExtensionData: make(map[string]*ext.ExtensionData, 0),
	}

	// Process any referenced extensions.
	//
	// NB: ExtensionReaders must be provided with the raw, encoded JSON
	// content in any object that might contain their key-value pairs, in
	// order for the standard golang json library to decode using struct
	// tags (e.g. for both json names/types and binding constraints like
	// `required`).  We cannot simply pass in the values for keys with the
	// extension's namespace as the prefix, since they might themselves not
	// be objects, and this would also result in a binding:"required" tag on
	// the extension's second-level key-value pairs being missed and thus
	// not correctly validated.
	//
	// Extensions are free to define key-value extensions to Global,
	// Captures[i], Annotations[i], *and* they can add new top-level objects
	// (peers to Global, Captures, Annotations).
	//
	// It would be nice if we could cache raw content for each of the keys
	// in the global objects, but this is not possible with the golang json
	// decoder.  We cannot even do a secondary parse pass where we decode
	// the keys in the top-level objects and leave their values undecoded
	// (RawMessage), because we do not know the field names in advance.
	//
	// Consequently, we leave optimized SigMF decoding to a future date when
	// we implement custom Unmarshaler logic for the core top-level objects.
	for _, ext := range m.Global.Extensions {
		if extReader, exists := extensionReaders[ext.Name]; !exists {
			if !ext.Optional {
				msg := fmt.Sprintf("cannot read sigmf metadata: required extension '%s' not supported", ext.Name)
				log.Debug().Msg(msg)
				return nil, errors.New(msg)
			} else {
				msg := fmt.Sprintf("reading partial sigmf metadata: optional extension '%s' not supported", ext.Name)
				log.Debug().Msg(msg)
			}
		} else if ed, rerr := extReader.ReadMetadata(data, ext, &ret.Decoding); rerr != nil {
			msg := fmt.Sprintf("failed read sigmf metadata extension (%s): %s", ext.Name, rerr.Error())
			log.Debug().Msg(msg)
			return nil, rerr
		} else {
			ret.ExtensionData[ext.Name] = ed
			msg := fmt.Sprintf("decoded extension (%s): %+v", ext.Name, ed)
			log.Debug().Msg(msg)
		}
	}

	return &ret, nil
}
