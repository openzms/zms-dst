// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package ntia

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin/binding"
	//"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext"
	ntiamodels "gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext/ntia/models"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/models"
)

type NtiaSensorMetadata struct {
	Global      ntiamodels.NtiaSensorGlobal
	Captures    []*ntiamodels.NtiaSensorCapture
	Annotations []*ntiamodels.NtiaSensorAnnotation
}

type NtiaSensorReader struct{}

func (NtiaSensorReader) GetNamespace() string {
	return "ntia-sensor"
}

// Reads ntia-sensor metadata from the Global, Captures, and Annotations
// primary fields in a SigMF metadata file.
//
// ntia-sensor only adds properties to the Global section.
func (NtiaSensorReader) ReadMetadata(data []byte, extension models.Extension, md *models.MetadataDecoding) (extdata *ext.ExtensionData, err error) {
	emd := NtiaSensorMetadata{}
	ed := ext.ExtensionData{
		Extension: extension,
		Value:     nil,
	}
	found := false
	for k, _ := range md.Partial.Global {
		if strings.HasPrefix(k, "ntia-sensor:") {
			found = true
			break
		}
	}
	// If the extension bits are not present, do not parse.
	if found {
		// Decode the extension's Global bits.
		ed.Value = &emd
		if err := json.Unmarshal(md.Raw.Global, &emd.Global); err != nil {
			return nil, fmt.Errorf("error processing sigmf metadata (ext ntia-sensor): %s", err.Error())
		}
		if err := binding.Validator.ValidateStruct(&emd.Global); err != nil {
			return nil, fmt.Errorf("error validating sigmf metadata (ext ntia-sensor): %s", err.Error())
		}
	}

	// Decode the extension's Capture bits, if any.
	for i, c := range md.Partial.Captures {
		found = false
		for k, _ := range c {
			if strings.HasPrefix(k, "ntia-sensor:") {
				found = true
				break
			}
		}
		if found {
			// If we haven't allocated space for any capture data yet, do
			// so.
			if len(emd.Captures) == 0 {
				emd.Captures = make([]*ntiamodels.NtiaSensorCapture, len(md.Partial.Captures))
			}

			// Decode the extension's Capture bits.
			if err := json.Unmarshal(md.Raw.Captures[i], &emd.Captures[i]); err != nil {
				return nil, fmt.Errorf("error processing sigmf metadata (ext ntia-sensor): %s", err.Error())
			}
			if err := binding.Validator.ValidateStruct(emd.Captures[i]); err != nil {
				return nil, fmt.Errorf("error validating sigmf metadata (ext ntia-sensor): %s", err.Error())
			}
		}
	}

	// Attach inline extension data now that we have no errors.
	md.Metadata.Global.AttachExtensionValue("ntia-sensor", &emd.Global)
	//log.Debug().Msg(fmt.Sprintf("%+v", (ed.Value.(*NtiaSensorMetadata)).Global.Sensor))
	for i, c := range emd.Captures {
		if c != nil {
			md.Metadata.Captures[i].AttachExtensionValue("ntia-sensor", emd.Captures[i])
		}
	}

	// Update the return value now that we know we have no errors.
	ed.Value = &emd

	return &ed, nil
}
