// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-emitter.sigmf-ext.md
//
// Namespace: ntia-emitter

package models

import (
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/models"
)

type Emitter struct {
	Id              string              `json:"id" binding:"required"`
	Description     *string             `json:"description"`
	Power           *float64            `json:"power"`
	Antenna         *Antenna            `json:"antenna"`
	Transmitter     *HardwareSpec       `json:"transmitter"`
	CenterFrequency *float64            `json:"center_frequency"`
	Waveform        *Waveform           `json:"waveform"`
	Geolocation     *models.Geolocation `json:"geolocation"`
	Environment     *Environment        `json:"environment"`
}

type NtiaEmitterGlobal struct {
	Emitters []Emitter `json:"ntia-emitter:emitters" binding:"dive,required"`
}

type NtiaEmitterCapture struct {
}

type NtiaEmitterAnnotation struct {
}
