// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-scos.sigmf-ext.md
//
// Namespace: ntia-scos

package models

import (
	"time"
)

type Action struct {
	Name        string  `json:"name" binding:"required"`
	Description *string `json:"description"`
	Summary     *string `json:"summary"`
}

type ScheduleEntry struct {
	Id       string     `json:"id" binding:"required"`
	Name     string     `json:"name" binding:"required"`
	Start    *time.Time `json:"start"`
	Stop     *time.Time `json:"stop"`
	Interval *int       `json:"interval"`
	Priority *int       `json:"priority"`
	Roles    []string   `json:"roles"`
}

type NtiaScosGlobal struct {
	Schedule  *ScheduleEntry `json:"ntia-scos:schedule"`
	Action    *Action        `json:"ntia-scos:action"`
	Task      *int           `json:"ntia-scos:task"`
	Recording *int           `json:"ntia-scos:recording"`
}

type NtiaScosCapture struct {
}

type NtiaScosAnnotation struct {
}
