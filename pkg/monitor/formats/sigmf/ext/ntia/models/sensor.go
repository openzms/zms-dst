// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-sensor.sigmf-ext.md
//
// Namespace: ntia-sensor

package models

type Sensor struct {
	SensorSpec     *HardwareSpec   `json:"sensor_spec" binding:"required"`
	Antenna        *Antenna        `json:"antenna"`
	Preselector    *Preselector    `json:"preselector"`
	SignalAnalyzer *SignalAnalyzer `json:"signal_analyzer"`
	ComputerSpec   *HardwareSpec   `json:"computer_spec"`
	Mobile         *bool           `json:"mobile"`
	Environment    *Environment    `json:"environment"`
	SensorSha512   *string         `json:"sensor_sha512"`
}

type SignalAnalyzer struct {
	SiganSpec     *HardwareSpec `json:"sigan_spec"`
	FrequencyLow  *float64      `json:"frequency_low"`
	FrequencyHigh *float64      `json:"frequency_high"`
	NoiseFigure   *float64      `json:"noise_figure"`
	MaxPower      *float64      `json:"max_power"`
	A2dBits       *int          `json:"a2d_bits"`
}

type Preselector struct {
	PreselectorSpec *HardwareSpec `json:"preselector_spec"`
	CalSources      []CalSource   `json:"cal_sources"`
	Amplifiers      []Amplifier   `json:"amplifiers"`
	Filters         []Filter      `json:"filters"`
	RfPaths         []RfPath      `json:"rf_paths"`
}

type CalSource struct {
	CalSourceSpec *HardwareSpec `json:"cal_source_spec"`
	Type          *string       `json:"type"`
	Enr           *float64      `json:"enr"`
}

type Amplifier struct {
	AmplifierSpec *HardwareSpec `json:"amplifier_spec"`
	Gain          *float64      `json:"gain"`
	NoiseFigure   *float64      `json:"noise_figure"`
	MaxPower      *float64      `json:"max_power"`
}

type Filter struct {
	FilterSpec            *HardwareSpec `json:"filter_spec"`
	FrequencyLowPassband  *float64      `json:"frequency_low_passband"`
	FrequencyHighPassband *float64      `json:"frequency_high_passband"`
	FrequencyLowStopband  *float64      `json:"frequency_low_stopband"`
	FrequencyHighStopband *float64      `json:"frequency_high_stopband"`
}

type RfPath struct {
	Id          string  `json:"id" binding:"required"`
	CalSourceId *string `json:"cal_source_id"`
	FilterId    *string `json:"filter_id"`
	AmplifierId *string `json:"amplifier_id"`
}

type Calibration struct {
	Datetime              *string  `json:"datetime"`
	Gain                  *float64 `json:"gain"`
	NoiseFigure           *float64 `json:"noise_figure"`
	OneDbCompressionPoint *float64 `json:"1db_compression_point"`
	Enbw                  *float64 `json:"enbw"`
	MeanNoisePower        *float64 `json:"mean_noise_power"`
	MeanNoisePowerUnits   *string  `json:"mean_noise_power_units"`
	Reference             *string  `json:"reference"`
	Temperature           *float64 `json:"temperature"`
}

type SiganSettings struct {
	Gain           *float64 `json:"gain"`
	ReferenceLevel *float64 `json:"reference_level"`
	Attenuation    *float64 `json:"attenuation"`
	PreampEnable   *bool    `json:"preamp_enable"`
}

type NtiaSensorGlobal struct {
	Sensor *Sensor `json:"ntia-sensor:sensor"`
}

type NtiaSensorCapture struct {
	Duration          *int           `json:"ntia-sensor:duration"`
	Overload          *bool          `json:"ntia-sensor:overload"`
	SensorCalibration *Calibration   `json:"ntia-sensor:sensor_calibration"`
	SiganCalibration  *Calibration   `json:"ntia-sensor:sigan_calibration"`
	SiganSettings     *SiganSettings `json:"ntia-sensor:sigan_settings"`
}

type NtiaSensorAnnotation struct {
}
