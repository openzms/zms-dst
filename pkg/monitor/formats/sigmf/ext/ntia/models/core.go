// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-core.sigmf-ext.md
//
// Namespace: ntia-core

package models

type HardwareSpec struct {
	Id                      string  `json:"id" binding:"required"`
	Model                   *string `json:"model"`
	Version                 *string `json:"version"`
	Description             *string `json:"description"`
	SupplementalInformation *string `json:"supplemental_information"`
}

type Antenna struct {
	AntennaSpec              HardwareSpec `json:"antenna_spec" binding:"required"`
	Type                     *string      `json:"type"`
	FrequencyLow             *float64     `json:"frequency_low"`
	FrequencyHigh            *float64     `json:"frequency_high"`
	Polarization             *string      `json:"polarization"`
	CrossPolarDiscrimination *float64     `json:"cross_polar_discrimination"`
	Gain                     *float64     `json:"gain"`
	HorizontalGainPattern    []float64    `json:"horizontal_gain_pattern"`
	VerticalGainPattern      []float64    `json:"vertical_gain_pattern"`
	HorizontalBeamwidth      *float64     `json:"horizontal_beamwidth"`
	VerticalBeamwidth        *float64     `json:"vertical_beamwidth"`
	VoltageStandingWaveRatio *float64     `json:"voltage_standing_wave_ratio"`
	CableLoss                *float64     `json:"cable_loss"`
	Steerable                *bool        `json:"steerable"`
	AzimuthAngle             *float64     `json:"azimuth_angle"`
	ElevationAngle           *float64     `json:"elevation_angle"`
}

type NtiaCoreGlobal struct {
	Classification string `json:"ntia-core:classification" binding:"required"`
}

type NtiaCoreCapture struct {
}

type NtiaCoreAnnotation struct {
}
