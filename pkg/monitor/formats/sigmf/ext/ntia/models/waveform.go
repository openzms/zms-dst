// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-waveform.sigmf-ext.md
//
// Namespace: ntia-waveform

package models

type Waveform struct {
	Model       *string `json:"model"`
	Description *string `json:"description"`
}

type Ieee80211pWaveform struct {
	Waveform
	InfoBitGeneration       *string  `json:"info_bit_generation"`
	CodingRate              []int    `json:"coding_rate"`
	PacketLength            int      `json:"packet_length"`
	Modulation              *string  `json:"modulation"`
	Encoder                 *string  `json:"encoder"`
	NumberOfSubcarriers     *int     `json:"number_of_subcarriers"`
	NumberOfDataSubcarriers *int     `json:"number_of_data_subcarriers"`
	NumberOfPilots          *int     `json:"number_of_pilots"`
	CyclicPrefix            *int     `json:"cyclic_prefix"`
	ShortInterFrameSpace    *float64 `json:"short_inter_frame_space"`
	PreambleFrame           []int    `json:"preamble_frame"`
	NumberOfInfoBits        *int     `json:"number_of_info_bits"`
	SignalToNoiseRatio      *float64 `json:"signal_to_noise_ratio"`
}

type NtiaWaveformGlobal struct {
}

type NtiaWaveformCaptures struct {
}

type NtiaWaveformAnnotations struct {
}
