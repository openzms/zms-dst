// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-environment.sigmf-ext.md
//
// Namespace: ntia-environment

package models

type Environment struct {
	Category    *string  `json:"category"`
	Temperature *float64 `json:"temperature"`
	Humidity    *float64 `json:"humidity"`
	Weather     *string  `json:"weather"`
	Description *string  `json:"description"`
}

type NtiaEnvironmentGlobal struct {
}

type NtiaEnvironmentCapture struct {
}

type NtiaEnvironmentAnnotation struct {
}
