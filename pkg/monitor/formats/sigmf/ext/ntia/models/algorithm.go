// https://github.com/NTIA/sigmf-ns-ntia/blob/master/ntia-algorithm.sigmf-ext.md
//
// Namespace: ntia-algorithm

package models

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/gin/binding"
	//"github.com/rs/zerolog/log"
)

type Graph struct {
	Name        string    `json:"name" binding:"required"`
	Series      []string  `json:"series"`
	Length      *int      `json:"length" binding:"required"`
	XUnits      *string   `json:"x_units"`
	XAxis       []string  `json:"x_axis"`
	XStart      []float64 `json:"x_start"`
	XStop       []float64 `json:"x_stop"`
	XStep       []float64 `json:"x_step"`
	YUnits      *string   `json:"y_units"`
	YAxis       []string  `json:"y_axis"`
	YStart      []float64 `json:"y_start"`
	YStop       []float64 `json:"y_stop"`
	YStep       []float64 `json:"y_step"`
	Processing  []string  `json:"processing"`
	Reference   *string   `json:"reference"`
	Description *string   `json:"description"`
}

type FilterType string

const (
	FilterTypeFIR FilterType = "FIR"
	FilterTypeIIR FilterType = "IIR"
)

type ProcessingInfoInterface interface {
	GetType() string
	GetId() string
}

type DigitalFilter struct {
	Type                    string     `json:"type" binding:"required,oneof=DigitalFilter"`
	Id                      string     `json:"id" binding:"required"`
	FilterType              FilterType `json:"filter_type"`
	FeedforwardCoefficients []float64  `json:"feedforward_coefficients"`
	FeedbackCoefficients    []float64  `json:"feedback_coefficients"`
	AttenuationCutoff       *float64   `json:"attenuation_cutoff"`
	FrequencyCutoff         *float64   `json:"frequency_cutoff"`
	Description             *string    `json:"description"`
}

func (df DigitalFilter) GetType() string { return df.Type }
func (df DigitalFilter) GetId() string   { return df.Id }

type Dft struct {
	Type                     string  `json:"type" binding:"required,oneof=DFT"`
	Id                       string  `json:"id" binding:"required"`
	EquivalentNoiseBandwidth float64 `json:"equivalent_noise_bandwidth"`
	Samples                  int     `json:"samples"`
	Dfts                     int     `json:"dfts"`
	Window                   string  `json:"window"`
	Baseband                 bool    `json:"baseband"`
	Description              *string `json:"description"`
}

func (dft Dft) GetType() string { return dft.Type }
func (dft Dft) GetId()   string { return dft.Id }

type ProcessingInfo struct {
	Type string `json:"type" binding:"required"`
	Id   string `json:"id" binding:"required"`
}

func (pi ProcessingInfo) GetType() string { return pi.Type }
func (pi ProcessingInfo) GetId()   string { return pi.Id }

type ProcessingInfoArray []ProcessingInfoInterface

type NtiaAlgorithmGlobal struct {
	DataProducts   []Graph             `json:"ntia-algorithm:data_products"`
	Processing     []string            `json:"ntia-algorithm:processing"`
	// Oneof Dft or DigitalFilter
	ProcessingInfo ProcessingInfoArray `json:"ntia-algorithm:processing_info"`
}

func (pia *ProcessingInfoArray) UnmarshalJSON(data []byte) error {
	// Scan into an array, then break that down.
	tmp := make([]json.RawMessage, 0)
	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}
	for _, ti := range tmp {
		var pi ProcessingInfo
		if err := json.Unmarshal(ti, &pi); err != nil {
			return err
		} else if err := binding.Validator.ValidateStruct(&pi); err != nil {
			return fmt.Errorf("error validating sigmf metadata: %s", err.Error())
		}
		if pi.Type == "DFT" {
			var dft Dft
			if err := json.Unmarshal(ti, &dft); err != nil {
				return err
			} else if err := binding.Validator.ValidateStruct(&dft); err != nil {
				return fmt.Errorf("error validating sigmf metadata: %s", err.Error())
			}
			*pia = append(*pia, dft)
		} else if pi.Type == "DigitalFilter" {
			var dft DigitalFilter
			if err := json.Unmarshal(ti, &dft); err != nil {
				return err
			} else if err := binding.Validator.ValidateStruct(&dft); err != nil {
				return fmt.Errorf("error validating sigmf metadata: %s", err.Error())
			}
			*pia = append(*pia, dft)
		} else {
			*pia = append(*pia, pi)
		}
	}

	return nil
}

type NtiaAlgorithmCapture struct {
}

type NtiaAlgorithmAnnotation struct {
}
