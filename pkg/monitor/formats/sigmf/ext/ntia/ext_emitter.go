// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package ntia

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gin-gonic/gin/binding"
	//"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext"
	ntiamodels "gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/ext/ntia/models"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/models"
)

type NtiaEmitterMetadata struct {
	Global      ntiamodels.NtiaEmitterGlobal
	Captures    []*ntiamodels.NtiaEmitterCapture
	Annotations []*ntiamodels.NtiaEmitterAnnotation
}

type NtiaEmitterReader struct{}

func (NtiaEmitterReader) GetNamespace() string {
	return "ntia-emitter"
}

// Reads ntia-emitter metadata from the Global, Captures, and Annotations
// primary fields in a SigMF metadata file.
//
// ntia-emitter only adds properties to the Global section.
func (NtiaEmitterReader) ReadMetadata(data []byte, extension models.Extension, md *models.MetadataDecoding) (extdata *ext.ExtensionData, err error) {
	emd := NtiaEmitterMetadata{}
	ed := ext.ExtensionData{
		Extension: extension,
		Value:     nil,
	}
	found := false
	for k, _ := range md.Partial.Global {
		if strings.HasPrefix(k, "ntia-emitter:") {
			found = true
			break
		}
	}
	// If the extension bits are not present, return
	if !found {
		return &ed, nil
	}

	// Decode the Global extension bits.
	ed.Value = &emd
	if err := json.Unmarshal(md.Raw.Global, &emd.Global); err != nil {
		return nil, fmt.Errorf("error processing sigmf metadata (ext ntia-emitter): %s", err.Error())
	}
	if err := binding.Validator.ValidateStruct(&emd.Global); err != nil {
		return nil, fmt.Errorf("error validating sigmf metadata (ext ntia-emitter): %s", err.Error())
	}

	md.Metadata.Global.AttachExtensionValue("ntia-emitter", &emd.Global)

	return &ed, nil
}
