// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package ext

import (
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/formats/sigmf/models"
)

type ExtensionData struct {
	Extension models.Extension
	Value     interface{}
	//Global      map[string]interface{}
	//Captures    map[string]interface{}
	//Annotations map[string]interface{}
}

type ExtensionReader interface {
	GetNamespace() (namespace string)
	// Readers are given the entire content of the SigMF metadata file.  This is necessary because extensions may place prefixed keys into each of the `global`, `captures`, and `annotations` keys
	ReadMetadata(data []byte, extension models.Extension, md *models.MetadataDecoding) (extensionData *ExtensionData, err error)
}
