// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package sigmf

import (
	"encoding/json"
	"fmt"
	"os"
	"testing"

	"github.com/rs/zerolog"
)

var doDebug bool = false

func init() {
	if os.Getenv("TEST_DEBUG") == "1" {
		doDebug = true
	}
	if doDebug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}
}

func pfj(i interface{}) string {
	if doDebug {
		b, _ := json.MarshalIndent(i, "  ", "  ")
		return string(b)
	} else {
		return fmt.Sprintf("%+v", i)
	}
}

func TestValid(t *testing.T) {

	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le"
  },
  "captures": [],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestInvalidGlobal(t *testing.T) {
	data := []byte(`
{
  "global": {},
  "captures": [],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err == nil {
		t.Fatalf(`sigmf.ReadMetadataFile did not fail (%+v)!`, mf)
	} else {
		t.Logf(`sigmf.ReadMetadataFile failed as expected: %v`, err)
	}
}

func TestValidCapture(t *testing.T) {

	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le"
  },
  "captures": [
    {
      "core:sample_start": 0
    }
  ],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestInvalidCapture(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      }
    ]
  },
  "captures": [{"foo":1}],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err == nil {
		t.Fatalf(`sigmf.ReadMetadataFile did not fail (%+v)!`, mf)
	} else {
		t.Logf(`sigmf.ReadMetadataFile failed as expected: %v`, err)
	}
}

func TestValidNtiaCore(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      }
    ],
    "ntia-core:classification": "UNCLASSIFIED"
  },
  "captures": [],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaSensor(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      },
      {
        "name" : "ntia-sensor",
        "version" : "v2.0.0",
        "optional" : false
      }
    ],
    "ntia-sensor:sensor" : {
      "sensor_spec" : {
        "id" : "Greyhound_1",
        "model" : "Example model",
        "version" : "v1.0",
        "description" : "Example description",
        "supplemental_information" : "Example supplemental information"
      },
      "antenna" : {
        "antenna_spec" : {
          "id" : "123-xyzpdq",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "type" : "omnidirectional",
        "frequency_low" : 3.0E8,
        "frequency_high" : 3.0E9,
        "gain" : 2.0,
        "polarization" : "vertical",
        "cross_polar_discrimination" : 9.1,
        "horizontal_gain_pattern" : [ 1.0, 2.0, 3.0 ],
        "vertical_gain_pattern" : [ 4.0, 5.0, 6.0 ],
        "horizontal_beamwidth" : 360.0,
        "vertical_beamwidth" : 10.0,
        "voltage_standing_wave_ratio" : 1.5,
        "cable_loss" : 1.0,
        "steerable" : false,
        "azimuth_angle" : 0.0,
        "elevation_angle" : 5.0
      },
      "preselector" : {
        "preselector_spec" : {
          "id" : "preselector_1",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "cal_sources" : [ {
          "cal_source_spec" : {
            "id" : "noise_diode_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "type" : "Calibrated noise diode",
          "enr" : 15.0
        } ],
        "filters" : [ {
          "filter_spec" : {
            "id" : "filter_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "frequency_low_passband" : 7.0E8,
          "frequency_high_passband" : 7.5E8,
          "frequency_low_stopband" : 7.0E8,
          "frequency_high_stopband" : 7.5E8
        } ],
        "amplifiers" : [ {
          "amplifier_spec" : {
            "id" : "lna_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "gain" : 30.0,
          "noise_figure" : 2.5,
          "max_power" : 35.0
        } ],
        "rf_paths" : [ {
          "id" : "path_1",
          "cal_source_id" : "noise_diode_1",
          "filter_id" : "filter_1",
          "amplifier_id" : "lna_1"
        } ]
      },
      "signal_analyzer" : {
        "sigan_spec" : {
          "id" : "875649305NLDKDJN",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "frequency_low" : 1.0E8,
        "frequency_high" : 7.0E8,
        "noise_figure" : 20.0,
        "max_power" : 35.0,
        "a2d_bits" : 16
      },
      "computer_spec" : {
        "id" : "Example_PC",
        "model" : "Example model",
        "version" : "v1.0",
        "description" : "Example description",
        "supplemental_information" : "Example supplemental information"
      },
      "mobile" : false,
      "environment" : {
        "temperature" : 100.0,
        "humidity" : 50.0,
        "weather" : "overcast",
        "category" : "outdoor",
        "description" : "A fake example environment"
      },
      "sensor_sha512" : "657bd59b8e46609411b9ba53d77fbc1dee75885fc6f3e4a744c6cfd80e2d85279a940f3e749733e91a2289cf728b83e0d76befb44b356d7933dc236f8f742556"
    },
    "ntia-core:classification": "UNCLASSIFIED"
  },
  "captures": [ {
    "core:sample_start" : 0,
    "ntia-sensor:overload" : false,
    "ntia-sensor:sigan_calibration" : {
      "gain" : 30.0,
      "temperature" : 28.0,
      "datetime" : "2023-05-31T19:59:19.651Z",
      "enbw" : 1.0E7,
      "reference" : "antenna terminal",
      "noise_figure" : 4.9,
      "1db_compression_point" : 33.0,
      "mean_noise_power" : -100.0,
      "mean_noise_power_units" : "dBm"
    },
    "ntia-sensor:sensor_calibration" : {
      "gain" : 30.0,
      "temperature" : 28.0,
      "datetime" : "2023-05-31T19:59:19.651Z",
      "enbw" : 1.0E7,
      "reference" : "antenna terminal",
      "noise_figure" : 4.9,
      "1db_compression_point" : 33.0,
      "mean_noise_power" : -100.0,
      "mean_noise_power_units" : "dBm"
    },
    "ntia-sensor:duration" : 100,
    "ntia-sensor:sigan_settings" : {
      "attenuation" : 10.0,
      "gain" : 15.0,
      "reference_level" : -33.0,
      "preamp_enable" : true
    }
  } ],
  "annotations": []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaScos(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      },
      {
        "name" : "ntia-scos",
        "version" : "v1.0.0",
        "optional" : false
      }
    ],
    "ntia-scos:task" : 1,
    "ntia-scos:recording" : 1,
    "ntia-scos:schedule" : {
      "roles" : [ "admin", "user" ],
      "id" : "m4s_action_id",
      "name" : "M4S_Every_Second",
      "stop" : "2023-05-31T19:57:33.341Z",
      "interval" : 1,
      "priority" : 10,
      "start" : "2023-05-31T19:57:33.341Z"
    },
    "ntia-scos:action" : {
      "name" : "example_acquire_m4s",
      "description" : "Placeholder text for a high-detail action description",
      "summary" : "Example description of an M4S detection action"
    },
    "ntia-core:classification" : "UNCLASSIFIED"
  },
  "captures" : [],
  "annotations" : []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaEnvironmentEmitter(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      },
      {
        "name" : "ntia-environment",
        "version" : "v1.0.0",
        "optional" : false
      },
      {
        "name" : "ntia-sensor",
        "version" : "v2.0.0",
        "optional" : false
      },
      {
        "name" : "ntia-emitter",
        "version" : "v2.0.0",
        "optional" : false
      }
    ],
    "ntia-core:classification": "UNCLASSIFIED",
    "ntia-sensor:sensor" : {
      "sensor_spec" : {
        "id" : "Greyhound_1",
        "model" : "Example model",
        "version" : "v1.0",
        "description" : "Example description",
        "supplemental_information" : "Example supplemental information"
      },
      "antenna" : {
        "antenna_spec" : {
          "id" : "123-xyzpdq",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "type" : "omnidirectional",
        "frequency_low" : 3.0E8,
        "frequency_high" : 3.0E9,
        "gain" : 2.0,
        "polarization" : "vertical",
        "cross_polar_discrimination" : 9.1,
        "horizontal_gain_pattern" : [ 1.0, 2.0, 3.0 ],
        "vertical_gain_pattern" : [ 4.0, 5.0, 6.0 ],
        "horizontal_beamwidth" : 360.0,
        "vertical_beamwidth" : 10.0,
        "voltage_standing_wave_ratio" : 1.5,
        "cable_loss" : 1.0,
        "steerable" : false,
        "azimuth_angle" : 0.0,
        "elevation_angle" : 5.0
      },
      "preselector" : {
        "preselector_spec" : {
          "id" : "preselector_1",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "cal_sources" : [ {
          "cal_source_spec" : {
            "id" : "noise_diode_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "type" : "Calibrated noise diode",
          "enr" : 15.0
        } ],
        "filters" : [ {
          "filter_spec" : {
            "id" : "filter_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "frequency_low_passband" : 7.0E8,
          "frequency_high_passband" : 7.5E8,
          "frequency_low_stopband" : 7.0E8,
          "frequency_high_stopband" : 7.5E8
        } ],
        "amplifiers" : [ {
          "amplifier_spec" : {
            "id" : "lna_1",
            "model" : "Example model",
            "version" : "v1.0",
            "description" : "Example description",
            "supplemental_information" : "Example supplemental information"
          },
          "gain" : 30.0,
          "noise_figure" : 2.5,
          "max_power" : 35.0
        } ],
        "rf_paths" : [ {
          "id" : "path_1",
          "cal_source_id" : "noise_diode_1",
          "filter_id" : "filter_1",
          "amplifier_id" : "lna_1"
        } ]
      },
      "signal_analyzer" : {
        "sigan_spec" : {
          "id" : "875649305NLDKDJN",
          "model" : "Example model",
          "version" : "v1.0",
          "description" : "Example description",
          "supplemental_information" : "Example supplemental information"
        },
        "frequency_low" : 1.0E8,
        "frequency_high" : 7.0E8,
        "noise_figure" : 20.0,
        "max_power" : 35.0,
        "a2d_bits" : 16
      },
      "computer_spec" : {
        "id" : "Example_PC",
        "model" : "Example model",
        "version" : "v1.0",
        "description" : "Example description",
        "supplemental_information" : "Example supplemental information"
      },
      "mobile" : false,
      "environment" : {
        "temperature" : 100.0,
        "humidity" : 50.0,
        "weather" : "overcast",
        "category" : "outdoor",
        "description" : "A fake example environment"
      },
      "sensor_sha512" : "657bd59b8e46609411b9ba53d77fbc1dee75885fc6f3e4a744c6cfd80e2d85279a940f3e749733e91a2289cf728b83e0d76befb44b356d7933dc236f8f742556"
    },
    "ntia-emitter:emitters" : [ {
      "id" : "EmitterXYZ",
      "description" : "A fictitious emitter to demonstrate the extensions format.",
      "power" : -60.0,
      "environment" : {
        "temperature" : 100.0,
        "humidity" : 50.0,
        "weather" : "overcast",
        "category" : "outdoor",
        "description" : "A fake example environment"
      }
    } ]
  },
  "captures" : [],
  "annotations" : []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaEmitter(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      },
      {
        "name" : "ntia-emitter",
        "version" : "v1.0.0",
        "optional" : false
      }
    ],
    "ntia-core:classification": "UNCLASSIFIED",
    "ntia-emitter:emitters" : [ {
      "id" : "N5182B-1",
      "description" : "Example emitter",
      "power" : 19.0,
      "antenna" : {
        "antenna_spec" : {
          "id" : "emitter-antenna",
          "model" : "Cobham_OA2-0.3-10.0V/1505",
          "description" : "Ultra Wide Band Omni Antenna",
          "supplemental_information" : "https://www.european-antennas.co.uk/media/1638/ds1505-060510.pdf"
        },
        "type" : "omni-directional",
        "frequency_low" : 3.0E8,
        "frequency_high" : 1.0E10,
        "gain" : 2.0,
        "polarization" : "horizontal",
        "cross_polar_discrimination" : 20.0,
        "horizontal_beamwidth" : 360.0,
        "vertical_beamwidth" : 65.0,
        "voltage_standing_wave_ratio" : 2.5,
        "cable_loss" : 0.15,
        "steerable" : false,
        "azimuth_angle" : 0.0,
        "elevation_angle" : 90.0
      },
      "transmitter" : {
        "id" : "tx_1",
        "model" : "N5182B",
        "version" : "v1.0.0",
        "description" : "Keysight MxG X-series RF Vector Signal Generator",
        "supplemental_information" : "https://www.keysight.com/us/en/assets/7018-03380/data-sheets/5991-0038.pdf"
      },
      "center_frequency" : 4.35E8,
      "waveform" : {
        "model" : "Waveform",
        "description" : "continuous wave"
      },
      "geolocation" : {
        "type" : "Point",
        "coordinates" : [ -79.980916, 40.304983 ]
      },
      "environment" : {
        "category" : "Underground, coal mine"
      }
    } ]
  },
  "captures" : [],
  "annotations" : []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaAlgorithm(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.0",
        "optional": false
      },
      {
        "name" : "ntia-algorithm",
        "version" : "v1.0.0",
        "optional" : false
      }
    ],
    "ntia-core:classification": "UNCLASSIFIED",
    "ntia-algorithm:processing_info": [
      {
        "type": "DigitalFilter",
        "id": "anti-alias-filter",
        "filter_type": "FIR",
        "feedforward_coefficients": [
          1.0,
          4.0,
          5.0,
          3.2
        ],
        "attenuation_cutoff": -3,
        "frequency_cutoff": 7500000,
        "description": "An example anti-aliasing filter, with dummy coefficients"
      }
    ],
    "ntia-algorithm:processing": ["anti-alias-filter"]
  },
  "captures" : [],
  "annotations" : []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}

func TestValidNtiaAlgorithm2(t *testing.T) {
	data := []byte(`
{
  "global": {
    "core:version": "1.0.0",
    "core:datatype": "rf64_le",
    "core:extensions": [
      {
        "name": "ntia-core",
        "version": "v2.0.1",
        "optional": false
      },
      {
        "name" : "ntia-algorithm",
        "version" : "v2.0.1",
        "optional" : false
      }
    ],
    "ntia-core:classification": "UNCLASSIFIED",
    "ntia-algorithm:data_products" : [ {
      "processing" : [ "psd_fft" ],
      "name" : "power_spectral_density",
      "description" : "Power spectral density with first and last 125 samples removed, computed from filtered data",
      "series" : [ "max", "mean" ],
      "length" : 625,
      "reference" : "calibration terminal",
      "x_units" : "Hz",
      "x_start" : [ -4992000.0 ],
      "x_stop" : [ 4992000.0 ],
      "x_step" : [ 16000.0 ],
      "y_units" : "dBm/Hz"
    }, {
      "name" : "power_vs_time",
      "description" : "Time series mean and max power computed from filtered data",
      "series" : [ "max", "mean" ],
      "length" : 400,
      "reference" : "calibration terminal",
      "y_units" : "dBm",
      "x_units" : "ms",
      "x_start" : [ 0.0 ],
      "x_stop" : [ 4000.0 ],
      "x_step" : [ 10.0 ]
    }, {
      "name" : "periodic_frame_power",
      "description" : "Periodic frame power with (min, max, mean) of mean and max detectors",
      "series" : [ "max-of-max", "mean-of-max", "min-of-max", "max-of-mean", "mean-of-mean", "min-of-mean" ],
      "length" : 560,
      "reference" : "calibration terminal",
      "x_units" : "ms",
      "x_start" : [ 0.0 ],
      "x_stop" : [ 10.0 ],
      "x_step" : [ 0.01785714285 ],
      "y_units" : "dBm"
    }, {
      "name" : "amplitude_probability_distribution",
      "description" : "Downsampled APD with 1 dBm bins",
      "length" : 151,
      "reference" : "calibration terminal",
      "y_units" : "dBm",
      "x_units" : "percent",
      "y_start" : [ -180.0 ],
      "y_stop" : [ -30.0 ],
      "y_step" : [ 1.0 ]
    } ],
    "ntia-algorithm:processing" : [ "iir_1" ],
    "ntia-algorithm:processing_info" : [ {
      "type" : "DFT",
      "id" : "psd_fft",
      "description" : "An example DFT object for 1000x 875-sample DFTs, using the flat top window and providing amplitudes at baseband frequencies.",
      "samples" : 875,
      "dfts" : 1000,
      "window" : "flattop",
      "baseband" : true,
      "equivalent_noise_bandwidth" : 51546.33
    }, {
      "type" : "DigitalFilter",
      "id" : "iir_1",
      "description" : "An example IIR 5 MHz lowpass filter",
      "filter_type" : "IIR",
      "feedforward_coefficients" : [ 0.22001755985277485, 1.8950858799155859, 8.083698129129006, 22.28438408611688, 43.93585109754826, 65.02462875088665, 73.93117717291233, 65.02462875088665, 43.93585109754826, 22.284384086116876, 8.083698129129006, 1.8950858799155852, 0.22001755985277482 ],
      "feedback_coefficients" : [ 1.0, 5.984606843057637, 19.199454663117216, 40.791247158352405, 63.2429677473874, 74.33110989910304, 67.69826765401139, 47.873252810169404, 26.149624421307166, 10.75285488653393, 3.2164061393115992, 0.6363986832562692, 0.07408086875619747 ],
      "attenuation_cutoff" : 80.0,
      "frequency_cutoff" : 5008000.0
    } ]
  },
  "captures" : [],
  "annotations" : []
}`)
	mf, err := ReadMetadataFile(data)
	if err != nil {
		t.Fatalf(`sigmf.ReadMetadataFile failed: %v`, err)
	} else {
		t.Logf(`sigmf.ReadMetadataFile success: %+v\n`, pfj(mf.Decoding.Metadata))
	}
}
