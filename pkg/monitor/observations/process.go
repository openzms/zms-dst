// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package observations

import (
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

type Processor interface {
	Check(o *store.Observation) error
	Update(o *store.Observation) (bool, error)
	Annotate(o *store.Observation) ([]*store.Annotation, error)
}

func ProcessorFactory(o *store.Observation) Processor {
	if o.Format == "psd-csv-ota" {
		return NewPowderProcessor()
	}
	if o.Format == "rfs-csv-inline" {
		return NewRfsProcessor()
	}

	return nil
}
