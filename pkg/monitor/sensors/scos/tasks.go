// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/rs/zerolog/log"
)

type TaskCompletedResponse struct {
	Count    int      `json:"count"`
	Next     string   `json:"next"`
	Previous string   `json:"previous"`
	Results  []Result `json:"results"`
}

func (client *ScosClient) GetTaskCompleted(name string) (resp *TaskCompletedResponse, err error) {
	var body []byte

	log.Debug().Msg(fmt.Sprintf("GetTaskCompleted name %s", name))
	hreq, err := http.NewRequest(
		"GET", client.BaseApiUrl+"/tasks/completed/"+name+"/", nil)
	hreq.Header.Set("Authorization", "Token "+client.Token)
	hreq.Header.Set("Content-Type", "application/json")
	log.Debug().Msg(fmt.Sprintf("GetTaskCompleted http req = %+v", hreq))
	var hresp *http.Response
	if hresp, err = client.Client.Do(hreq); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("GetTaskCompleted http resp = %+v", hresp))
	if hresp.StatusCode != 200 {
		return nil, NewScosClientError(errors.New(hresp.Status), hresp.StatusCode)
	}
	if body, err = ioutil.ReadAll(hresp.Body); err != nil {
		return nil, err
	}

	resp = new(TaskCompletedResponse)
	if err = json.Unmarshal(body, resp); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("TaskCompletedResponse resp = %+v", resp))
	return resp, nil
}

func (client *ScosClient) DeleteTaskCompleted(name string) (err error) {
	log.Debug().Msg(fmt.Sprintf("DeleteTaskCompleted name %s", name))
	hreq, err := http.NewRequest(
		"DELETE", client.BaseApiUrl+"/tasks/completed/"+name+"/", nil)
	hreq.Header.Set("Authorization", "Token "+client.Token)
	hreq.Header.Set("Content-Type", "application/json")
	log.Debug().Msg(fmt.Sprintf("DeleteTaskCompleted http req = %+v", hreq))
	var hresp *http.Response
	if hresp, err = client.Client.Do(hreq); err != nil {
		return err
	}
	log.Debug().Msg(fmt.Sprintf("DeleteTaskCompleted http resp = %+v", hresp))
	if hresp.StatusCode != 204 {
		return NewScosClientError(errors.New(hresp.Status), hresp.StatusCode)
	}
	return nil
}
