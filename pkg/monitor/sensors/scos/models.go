// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"time"
)

type Action struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Summary     string `json:"summary"`
}

type Geolocation struct {
	Type        string    `json:"type"`
	Coordinates []float64 `json:"coordinates"`
}

type SensorSpec struct {
	ID    string `json:"id"`
	Model string `json:"model"`
}

type Antenna struct {
	AntennaSpec SensorSpec `json:"antenna_spec"`
}

type SignalAnalyzer struct {
	SiganSpec SensorSpec `json:"sigan_spec"`
}

type ComputerSpec struct {
	ID    string `json:"id"`
	Model string `json:"model"`
}

type Sensor struct {
	SensorSpec     SensorSpec     `json:"sensor_spec"`
	Antenna        Antenna        `json:"antenna"`
	SignalAnalyzer SignalAnalyzer `json:"signal_analyzer"`
	ComputerSpec   ComputerSpec   `json:"computer_spec"`
	Mobile         bool           `json:"mobile"`
	SensorSha512   string         `json:"sensor_sha512"`
}

type DataProduct struct {
	Name        string    `json:"name"`
	Series      []string  `json:"series"`
	Length      int       `json:"length"`
	XUnits      string    `json:"x_units"`
	XStart      []float64 `json:"x_start"`
	XStop       []float64 `json:"x_stop"`
	XStep       []float64 `json:"x_step"`
	YUnits      string    `json:"y_units"`
	Reference   string    `json:"reference"`
	Description string    `json:"description"`
}

type Metadata struct {
	Global      Global       `json:"global"`
	Captures    []Capture    `json:"captures"`
	Annotations []Annotation `json:"annotations"`
}

type Global struct {
	CoreVersion                   string               `json:"core:version"`
	CoreExtensions                []Extension          `json:"core:extensions"`
	CoreRecorder                  string               `json:"core:recorder"`
	NtiaScosSchedule              NtiaScosSchedule     `json:"ntia-scos:schedule"`
	NtiaScosAction                NtiaScosAction       `json:"ntia-scos:action"`
	CoreGeolocation               CoreGeolocation      `json:"core:geolocation"`
	NtiaSensorSensorSpec          NtiaSensorSensorSpec `json:"ntia-sensor:sensor"`
	CoreSampleRate                float64              `json:"core:sample_rate"`
	NtiaScosTask                  int                  `json:"ntia-scos:task"`
	NtiaCoreClassification        string               `json:"ntia-core:classification"`
	NtiaSensorCalibrationDatetime string               `json:"ntia-sensor:calibration_datetime"`
	CoreDatatype                  string               `json:"core:datatype"`
	NtiaAlgorithmProcessing       []string             `json:"ntia-algorithm:processing"`
	NtiaAlgorithmProcessingInfo   []ProcessingInfo     `json:"ntia-algorithm:processing_info"`
	NtiaAlgorithmDataProducts     []DataProduct        `json:"ntia-algorithm:data_products"`
}

type Extension struct {
	Name     string `json:"name"`
	Version  string `json:"version"`
	Optional bool   `json:"optional"`
}

type NtiaScosSchedule struct {
	ID       string `json:"id"`
	Name     string `json:"name"`
	Start    string `json:"start"`
	Priority int    `json:"priority"`
}

type NtiaScosAction struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Summary     string `json:"summary"`
}

type CoreGeolocation struct {
	Type        string    `json:"type"`
	Coordinates []float64 `json:"coordinates"`
}

type NtiaSensorSensorSpec struct {
	ID    string `json:"id"`
	Model string `json:"model"`
}

type ProcessingInfo struct {
	Type                     string  `json:"type"`
	ID                       string  `json:"id"`
	EquivalentNoiseBandwidth float64 `json:"equivalent_noise_bandwidth"`
	Samples                  int     `json:"samples"`
	Dfts                     int     `json:"dfts"`
	Window                   string  `json:"window"`
	Baseband                 bool    `json:"baseband"`
	Description              string  `json:"description"`
}

type Capture struct {
	CoreFrequency               float64           `json:"core:frequency"`
	CoreDatetime                string            `json:"core:datetime"`
	NtiaSensorDuration          int               `json:"ntia-sensor:duration"`
	NtiaSensorOverload          bool              `json:"ntia-sensor:overload"`
	NtiaSensorSensorCalibration SensorCalibration `json:"ntia-sensor:sensor_calibration"`
	NtiaSensorSiganCalibration  SiganCalibration  `json:"ntia-sensor:sigan_calibration"`
	NtiaSensorSiganSettings     SiganSettings     `json:"ntia-sensor:sigan_settings"`
	CoreSampleStart             int               `json:"core:sample_start"`
}

type Annotation struct {
	SampleStart   int     `json:"sample_start"`
	SampleCount   int     `json:"sample_count"`
	Generator     string  `json:"generator"`
	Label         string  `json:"label"`
	Comment       string  `json:"comment"`
	FreqLowerEdge float64 `json:"freq_lower_edge"`
	FreqUpperEdge float64 `json:"freq_upper_edge"`
	Uuid          string  `json:"uuid"`
}

type SensorCalibration struct {
	Datetime    string  `json:"datetime"`
	Temperature float64 `json:"temperature"`
}

type SiganCalibration struct {
	Datetime              string  `json:"datetime"`
	NoiseFigure           float64 `json:"noise_figure"`
	OneDbCompressionPoint int     `json:"1db_compression_point"`
	Temperature           float64 `json:"temperature"`
}

type SiganSettings struct {
	Gain int `json:"gain"`
}

type Data struct {
	RecordingID int      `json:"recording_id"`
	Archive     string   `json:"archive"`
	Metadata    Metadata `json:"metadata"`
}

type Result struct {
	Self          string     `json:"self"`
	ScheduleEntry string     `json:"schedule_entry"`
	TaskID        int        `json:"task_id"`
	Status        string     `json:"status"`
	Detail        string     `json:"detail"`
	Started       time.Time  `json:"started"`
	Finished      *time.Time `json:"finished"`
	Duration      *string    `json:"duration"`
	Data          []Data     `json:"data"`
}

type StatusResponse struct {
	Scheduler               string    `json:"scheduler"`
	SystemTime              time.Time `json:"system_time"`
	StartTime               time.Time `json:"start_time"`
	LastCalibrationDatetime time.Time `json:"last_calibration_datetime"`
	DiskUsage               int       `json:"disk_usage"`
	DaysUp                  float64   `json:"days_up"`
	Healthy                 bool      `json:"healthy"`
}

type Schedule struct {
	Self         *string    `json:"self"`
	Name         string     `json:"name"`
	Action       string     `json:"action"`
	Priority     *int       `json:"priority"`
	Start        *time.Time `json:"start"`
	Stop         *time.Time `json:"stop"`
	RelativeStop *int       `json:"relative_stop"`
	Interval     *int       `json:"interval"`
	IsActive     *bool      `json:"is_active"`
	CallbackUrl  *string    `json:"callback_url"`
	NextTaskTime *time.Time `json:"next_task_time"`
	NextTaskId   *int       `json:"next_task_id"`
	Created      *time.Time `json:"created"`
	Modified     *time.Time `json:"modified"`
	Owner        *string    `json:"string"`
	TaskResults  *string    `json:"task_results"`
	ValidateOnly *bool      `json:"validate_only"`
}
