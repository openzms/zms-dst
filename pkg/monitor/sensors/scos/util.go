// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"fmt"

	"github.com/rs/zerolog/log"
)

func FindRangesBelowThreshold(data []float64, threshold float64, minLen int) (ranges [][2]int) {
	log.Debug().Msg(fmt.Sprintf("data: %+v", data))
	s := 0
	i := 0
	for i = 0; i < len(data); i += 1 {
		if data[i] > threshold {
			if (i-s) >= minLen && minLen > 0 {
				ranges = append(ranges, [2]int{s, i - 1})
			}
			s = i
		}
	}
	if (i-s) >= minLen && minLen > 0 {
		ranges = append(ranges, [2]int{s, i - 1})
	}
	return ranges
}
