// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/rs/zerolog/log"
	//"google.golang.org/grpc/codes"
	//"google.golang.org/grpc/status"
	//"google.golang.org/protobuf/types/known/timestamppb"
	//"gorm.io/gorm"
	"github.com/google/uuid"

	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
	//dst "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/monitor/observations"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

type ScosMonitorConfigAuth struct {
	Token string `json:"token"`
}

type ScosMonitorConfig struct {
	Auth ScosMonitorConfigAuth `json:"auth"`
	Url  string                `json:"url"`
}

func ScosClientFromMonitor(m *zmc.Monitor) (c *ScosClient, err error) {
	if m.Config == "" {
		return nil, fmt.Errorf("monitor config json missing")
	}

	var config ScosMonitorConfig
	if err := json.Unmarshal([]byte(m.Config), &config); err != nil {
		return nil, fmt.Errorf("scos monitor config invalid: %+v", err)
	}
	log.Debug().Msg(fmt.Sprintf("ScosMonitorConfig = %+v", config))

	c = NewScosClient(config.Url, config.Auth.Token, true)
	c.SetMonitor(m)

	return c, nil
}

func (c *ScosClient) CollectSpectrumFft(minFreq int64, maxFreq int64) (_ *store.Collection, err error) {
	if (minFreq < c.MinFreqFft || minFreq > c.MaxFreqFft) || (maxFreq < c.MinFreqFft || maxFreq > c.MaxFreqFft) {
		return nil, fmt.Errorf("unsupported fft frequency range")
	}

	timeMax := time.Unix(1<<63-62135596801, 999999999)
	epoch := time.Unix(0, 0).UTC()
	tlist := make([]string, 0, c.FftActionCount)
	for i := 0; i < c.FftActionCount; i += 1 {
		sar := ScheduleActionRequest{
			Name:   uuid.New().String(),
			Action: fmt.Sprintf("%s%d", c.FftActionPrefix, i),
		}
		tlist = append(tlist, sar.Name)
		var resp *ScheduleActionResponse
		if resp, err = c.ScheduleAction(&sar); err != nil {
			return nil, fmt.Errorf("failed to schedule action %+v: %+v", sar, err)
		}
		log.Debug().Msg(fmt.Sprintf("ScheduleAction resp = %+v", resp))
	}

	collection := &store.Collection{
		Id:          uuid.New(),
		MonitorId:   *c.MonitorId,
		Types:       c.Monitor.Types,
		Format:      "scos",
		Description: "SCOS DFT",
		MinFreq:     -1,
		MaxFreq:     -1,
		StartsAt:    timeMax,
		EndsAt:      &epoch,
	}
	//olist := make([]*Observation, 0, c.FftActionCount)
	for _, name := range tlist {
		var tresp *TaskCompletedResponse
		for i := 0; i < 30; i += 1 {
			if tresp, err = c.GetTaskCompleted(name); err != nil {
				log.Warn().Msg(fmt.Sprintf("failed to get completed task: %+v", err))
				time.Sleep(1 * time.Second)
			} else {
				if len(tresp.Results) == 0 || len(tresp.Results[0].Data) == 0 {
					log.Warn().Msg(fmt.Sprintf("failed to get data from completed task: %+v", err))
					time.Sleep(1 * time.Second)
				} else {
					break
				}
			}
		}
		if tresp == nil {
			log.Warn().Msg(fmt.Sprintf("failed to get completed task %+v", name))
			continue
		}
		log.Debug().Msg(fmt.Sprintf("TaskCompletedResponse resp = %+v", tresp))

		if o, err := ConvResultToObservation(&tresp.Results[0], c.Monitor); err != nil {
			log.Warn().Msg(fmt.Sprintf("failed to convert result to observation: %+v", err))
		} else {
			o.CollectionId = &collection.Id
			collection.Observations = append(collection.Observations, *o)

			if collection.MinFreq == -1 || o.MinFreq < collection.MinFreq {
				collection.MinFreq = o.MinFreq
			}
			if collection.MaxFreq == -1 || o.MaxFreq > collection.MaxFreq {
				collection.MaxFreq = o.MaxFreq
			}
			if o.StartsAt.Before(collection.StartsAt) {
				collection.StartsAt = o.StartsAt
			}
			if o.EndsAt != nil && o.EndsAt.After(*collection.EndsAt) {
				collection.EndsAt = o.EndsAt
			}
		}

		c.DeleteTaskCompleted(name)
		c.DeleteScheduleAction(name)
	}

	return collection, nil
}

func ConvResultToObservation(r *Result, m *zmc.Monitor) (o *store.Observation, err error) {
	var obs store.Observation
	var action = r.Data[0].Metadata.Global.NtiaScosAction
	var processingInfo = r.Data[0].Metadata.Global.NtiaAlgorithmProcessingInfo[0]
	var dataProducts = r.Data[0].Metadata.Global.NtiaAlgorithmDataProducts[0]

	obs.StartsAt = r.Started
	if r.Finished != nil {
		obs.EndsAt = r.Finished
	}
	obs.Types = "scos"
	obs.Format = "sigmf"
	obs.Description = action.Description
	obs.MinFreq = int64(dataProducts.XStart[0])
	obs.MaxFreq = int64(dataProducts.XStop[0])
	obs.FreqStep = int64(dataProducts.XStep[0])

	if processingInfo.Type == "DFT" {
		seriesLen := dataProducts.Length
		idx := strings.IndexByte(dataProducts.Description, ':')
		sa := strings.Split(dataProducts.Description[idx:], ",")
		sf := make([]float64, 0, seriesLen)
		// Select the proper series: minimum maximum mean median sample
		meanOffset := 2
		for _, sai := range sa[meanOffset*seriesLen : meanOffset*seriesLen+seriesLen] {
			f, _ := strconv.ParseFloat(sai, 64)
			sf = append(sf, f)
		}
		log.Debug().Msg(fmt.Sprintf("%d floats", len(sf)))

		//ranges := FindRangesBelowThreshold(sf, -59.0, 10)
		//log.Info().Msg(fmt.Sprintf("ranges: %+v", ranges))

		annValue := observations.PsdAnnotationValue{
			Stat:     "mean",
			Length:   seriesLen,
			MinFreq:  obs.MinFreq,
			MaxFreq:  obs.MaxFreq,
			FreqStep: obs.FreqStep,
			Series:   sf,
		}
		var encValue []byte
		if encValue, err = json.Marshal(annValue); err != nil {
			return nil, fmt.Errorf("failed to convert scos fft value into psd Annotation: %+v", err)
		}
		ann := store.Annotation{
			Type:  "psd-mean",
			Name:  "scos-dft-mean",
			Value: encValue,
		}
		obs.Annotations = append(obs.Annotations, ann)
	}

	return &obs, nil
}
