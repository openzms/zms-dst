// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package scos

import (
	"crypto/tls"
	"net/http"

	"github.com/google/uuid"

	zmc "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"
)

type ScosClient struct {
	BaseApiUrl      string
	Token           string
	Client          *http.Client
	MinFreqFft      int64
	MaxFreqFft      int64
	FftActionPrefix string
	FftActionCount  int
	Monitor         *zmc.Monitor
	MonitorId       *uuid.UUID
}

func NewScosClient(baseApiUrl string, token string, insecureTls bool) (client *ScosClient) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureTls},
	}
	httpClient := &http.Client{Transport: tr}

	client = &ScosClient{
		BaseApiUrl:      baseApiUrl,
		Token:           token,
		Client:          httpClient,
		MinFreqFft:      3350000000,
		MaxFreqFft:      3600000000,
		FftActionPrefix: "acquire_m4s_cband_",
		FftActionCount:  10,
	}

	return client
}

func (c *ScosClient) SetMonitor(m *zmc.Monitor) error {
	if idParsed, err := uuid.Parse(m.Id); err != nil {
		return err
	} else {
		c.MonitorId = &idParsed

	}
	c.Monitor = m

	return nil
}

type HttpError interface {
	error
	Code() int
}

type ScosClientError struct {
	err  error
	code int
}

func NewScosClientError(err error, code int) ScosClientError {
	return ScosClientError{err: err, code: code}
}

func (e ScosClientError) Error() string {
	return e.err.Error()
}

func (e ScosClientError) Code() int {
	return e.code
}
