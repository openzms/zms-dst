// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package propsim

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/google/uuid"
	"github.com/rs/zerolog/log"
	"google.golang.org/grpc"
	//"google.golang.org/grpc/codes"
	"google.golang.org/grpc/credentials/insecure"
	//"google.golang.org/grpc/status"
	"gorm.io/gorm"

	dst_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/dst/v1"
	event_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/event/v1"
	identity_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/identity/v1"
	propsim_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/propsim/v1"
	zmc_v1 "gitlab.flux.utah.edu/openzms/zms-api/go/zms/zmc/v1"

	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/client"
	"gitlab.flux.utah.edu/openzms/zms-lib-go/pkg/subscription"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/config"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/propsim/geoserver"
	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/store"
)

/*
type Feature struct {
	Name        string  `json:"name"`
	Description *string `json:"description"`
}

type ParameterDefinition struct {
	Name       string `json:"string"`
	Type       string `json:"type"`
	Desc       string `json:"desc"`
	IsRequired bool   `json:"is_required"`
}
*/

// This type wraps both an identity_v1.Service high-level OpenZMS service
// descriptor, and a propsim_v1.ServiceDescriptor, for exposure to our NB
// API.
type PropsimService struct {
	Id          string                        `json:"id"`
	Name        string                        `json:"name"`
	Description string                        `json:"description"`
	Desc        *propsim_v1.ServiceDescriptor `json:"desc"`

	service *identity_v1.Service        `json:"-"`
	client  propsim_v1.JobServiceClient `json:"-"`
}

type PropsimController struct {
	mutex    sync.RWMutex
	config   *config.Config
	db       *gorm.DB
	sm       *subscription.SubscriptionManager[*subscription.Event]
	services map[string]*PropsimService
	rclient  *client.ZmsClient
}

func NewPropsimController(serverConfig *config.Config, db *gorm.DB, sm *subscription.SubscriptionManager[*subscription.Event], c *client.ZmsClient) (pc PropsimController) {
	pc = PropsimController{
		config:  serverConfig,
		db:      db,
		sm:      sm,
		rclient: c,
	}
	pc.services = make(map[string]*PropsimService, 0)
	return pc
}

// Get a list of PropsimServices (+ServiceDescriptor) via the client.  We
// only refetch the ServiceDescriptor if the service is new, or if the
// service was more recently updated than our cache.
func (pc *PropsimController) GetPropsimServices(ctx context.Context) (err error, ret []*PropsimService) {
	services := pc.rclient.GetServices("propsim")
	if services == nil || len(services) < 1 {
		log.Debug().Msg(fmt.Sprintf("GetPropsimServices: no known propsim services"))
		return nil, nil
	}

	usl := make([]*identity_v1.Service, 0, len(services))
	ret = make([]*PropsimService, 0, len(services))

	pc.mutex.RLock()
	// Build a list of services requiring a refresh, and update them
	// asynchronously.
	for _, s := range services {
		if cs, ok := pc.services[s.Id]; ok {
			if cs.service.UpdatedAt == s.UpdatedAt || (cs.service.UpdatedAt != nil && s.UpdatedAt != nil && cs.service.UpdatedAt.AsTime() == s.UpdatedAt.AsTime()) {
				ret = append(ret, cs)
				continue
			}
		}
		usl = append(usl, s)
	}
	pc.mutex.RUnlock()

	// No updates required.
	if len(usl) < 1 {
		log.Debug().Msg(fmt.Sprintf("GetPropsimServices: no propsim services require updates"))
		return nil, ret
	}

	// Grab the ServiceDescriptor for the services needing an update, and
	// make it so.
	for _, us := range usl {
		if conn, err := grpc.Dial(
			us.Endpoint,
			grpc.WithTransportCredentials(insecure.NewCredentials()),
			//grpc.WithKeepaliveParams(keepalive.ClientParameters{PermitWithoutStream: true})
		); err != nil {
			log.Error().Err(err).Msg(fmt.Sprintf("failed to connect to propsim service (%s): %v", us.Endpoint, err))
			continue
		} else {
			c := propsim_v1.NewJobServiceClient(conn)

			var resp *propsim_v1.GetServiceDescriptorResponse
			if resp, err = c.GetServiceDescriptor(ctx, &propsim_v1.GetServiceDescriptorRequest{}); err != nil {
				log.Error().Err(err).Msg(fmt.Sprintf("failed to GetServiceDescriptor from propsim service (%s): %v", us.Endpoint, err))
				continue
			} else if resp == nil || resp.Desc == nil {
				log.Error().Err(err).Msg(fmt.Sprintf("bogus reply to GetServiceDescriptor from propsim service (%s): %v", us.Endpoint, err))
				continue
			}

			cs := PropsimService{
				Id:          us.Id,
				Name:        us.Name,
				Description: us.Description,
				Desc:        resp.Desc,
				service:     us,
				client:      c,
			}

			pc.mutex.Lock()
			pc.services[us.Id] = &cs
			pc.mutex.Unlock()

			log.Debug().Msg(fmt.Sprintf("updated ServiceDescriptor for propsim service: %+v", cs))

			ret = append(ret, &cs)
		}
	}

	return nil, ret
}

type PropsimParameterValue struct {
	Name  string      `json:"name"`
	Value interface{} `json:"value"`
}

type PropsimConfig struct {
	Parameters []PropsimParameterValue `json:"parameters"`
}

// Converts a `PropsimConfig` object into a propsim_v1.ParameterValue slice,
// ready to submit as part of a propsim_v1.CreateJobRequest.  This allows customizations of specific parameters
func (pc *PropsimConfig) ToProto(desc *propsim_v1.ServiceDescriptor) (out []*propsim_v1.ParameterValue, err error) {
	out = make([]*propsim_v1.ParameterValue, 0, 0)

	for _, p := range pc.Parameters {
		var pd *propsim_v1.ParameterDefinition
		for _, pdt := range desc.Parameters {
			if pdt.Name == p.Name {
				pd = pdt
				break
			}
		}
		if pd == nil {
			return nil, fmt.Errorf("unknown parameter %s", p.Name)
		}
		switch t := p.Value.(type) {
		case bool:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_BOOL {
				return nil, fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
			pv := propsim_v1.ParameterValue{
				Name:  p.Name,
				Value: &propsim_v1.ParameterValue_B{B: p.Value.(bool)},
			}
			out = append(out, &pv)
		case float64:
			if pd.Ptype == propsim_v1.ParameterType_PARAMETER_TYPE_INT {
				pv := propsim_v1.ParameterValue{
					Name:  p.Name,
					Value: &propsim_v1.ParameterValue_I{I: p.Value.(int64)},
				}
				out = append(out, &pv)
			} else if pd.Ptype == propsim_v1.ParameterType_PARAMETER_TYPE_DOUBLE {
				pv := propsim_v1.ParameterValue{
					Name:  p.Name,
					Value: &propsim_v1.ParameterValue_D{D: p.Value.(float64)},
				}
				out = append(out, &pv)

			} else {
				return nil, fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
			pv := propsim_v1.ParameterValue{
				Name:  p.Name,
				Value: &propsim_v1.ParameterValue_S{S: p.Value.(string)},
			}
			out = append(out, &pv)
		case string:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_STRING {
				return nil, fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
			pv := propsim_v1.ParameterValue{
				Name:  p.Name,
				Value: &propsim_v1.ParameterValue_S{S: p.Value.(string)},
			}
			out = append(out, &pv)
		default:
			return nil, fmt.Errorf("invalid propsim parameter type %+v", t)
		}
	}

	return out, nil
}

func CheckParameters(values []*propsim_v1.ParameterValue, desc *propsim_v1.ServiceDescriptor) error {
	// Bookkeep values for final check that all is_required params are
	// present.
	pvd := make(map[string]interface{})

	for _, p := range values {
		var pd *propsim_v1.ParameterDefinition
		for _, pdt := range desc.Parameters {
			if pdt.Name == p.Name {
				pd = pdt
				break
			}
		}
		if pd == nil {
			return fmt.Errorf("unknown parameter %s", p.Name)
		}
		pvd[p.Name] = nil

		switch t := p.Value.(type) {
		case *propsim_v1.ParameterValue_B:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_BOOL {
				return fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
		case *propsim_v1.ParameterValue_D:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_DOUBLE {
				return fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
		case *propsim_v1.ParameterValue_I:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_INT {
				return fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
		case *propsim_v1.ParameterValue_S:
			if pd.Ptype != propsim_v1.ParameterType_PARAMETER_TYPE_STRING {
				return fmt.Errorf("type mismatch for parameter %s", p.Name)
			}
		default:
			return fmt.Errorf("invalid propsim parameter type %+v", t)
		}
	}

	// Check that all required parameters are set.
	for _, pdt := range desc.Parameters {
		if !pdt.IsRequired {
			continue
		} else if _, ok := pvd[pdt.Name]; !ok {
			return fmt.Errorf("required parameter '%s' not provided", pdt.Name)
		}
	}

	return nil
}

func (pc *PropsimController) CreateJob(ctx context.Context, p *store.Propsim) (*store.PropsimJob, error) {
	if p.ServiceId == nil {
		return nil, fmt.Errorf("no service specified")
	}
	var ps *PropsimService
	pc.mutex.RLock()
	if _ps, ok := pc.services[p.ServiceId.String()]; !ok {
		pc.mutex.RUnlock()
		return nil, fmt.Errorf("unknown service")
	} else {
		ps = _ps
	}
	pc.mutex.RUnlock()

	// If config, parse config vs service descriptor; convert into
	// ParameterValues.
	var values []*propsim_v1.ParameterValue

	if p.Config != nil {
		var pConfig PropsimConfig
		if err := json.Unmarshal([]byte(p.Config.String()), &pConfig); err != nil {
			return nil, err
		}
		if v, err := pConfig.ToProto(ps.Desc); err != nil {
			return nil, err
		} else {
			values = v
		}
	} else {
		values = make([]*propsim_v1.ParameterValue, 0, 0)
	}

	if p.RadioPortId != nil {
		var radioPort *zmc_v1.RadioPort
		if _radioPort, err := pc.rclient.GetRadioPort(ctx, p.RadioPortId, true); err != nil {
			return nil, fmt.Errorf("failure getting radio port: %s", err.Error())
		} else {
			radioPort = _radioPort
		}
		var radio *zmc_v1.Radio
		var radioId uuid.UUID
		if _radioId, err := uuid.Parse(radioPort.RadioId); err == nil {
			radioId = _radioId
		} else {
			return nil, fmt.Errorf("error decoding radio port radio id: %s", err.Error())
		}
		if _radio, err := pc.rclient.GetRadio(ctx, &radioId, true); err != nil {
			return nil, fmt.Errorf("failure getting radio: %s", err.Error())
		} else {
			radio = _radio
		}

		if radioPort.AntennaLocation == nil {
			return nil, fmt.Errorf("radio port has no location; cannot simulate")
		}

		// Add RadioPort/Antenna details to ParameterValues.
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "radio_name",
			Value: &propsim_v1.ParameterValue_S{S: radioPort.Name},
		})
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "radio_lat",
			Value: &propsim_v1.ParameterValue_D{D: radioPort.AntennaLocation.Y},
		})
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "radio_lon",
			Value: &propsim_v1.ParameterValue_D{D: radioPort.AntennaLocation.X},
		})
		if radioPort.AntennaElevationDelta != nil {
			values = append(values, &propsim_v1.ParameterValue{
				Name:  "txheight",
				Value: &propsim_v1.ParameterValue_D{D: float64(*radioPort.AntennaElevationDelta)},
			})
		} else if radioPort.AntennaLocation.Z != nil && radio.Location != nil && radio.Location.Z != nil {
			locationDelta := *radioPort.AntennaLocation.Z - *radio.Location.Z
			values = append(values, &propsim_v1.ParameterValue{
				Name:  "txheight",
				Value: &propsim_v1.ParameterValue_D{D: locationDelta},
			})
		}
		if radioPort.Antenna != nil && radioPort.Antenna.Model != "" {
			model := radioPort.Antenna.Model
			if strings.HasPrefix(model, "VVSSP") {
				model = "VVSSP"
			}
			values = append(values, &propsim_v1.ParameterValue{
				Name:  "antenna_model",
				Value: &propsim_v1.ParameterValue_S{S: model},
			})
		}
	}
	if p.Freq != nil {
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "freq",
			Value: &propsim_v1.ParameterValue_D{D: float64(*p.Freq) / 1000000},
		})
	}
	if p.Resolution != nil {
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "resolution",
			Value: &propsim_v1.ParameterValue_D{D: float64(*p.Resolution)},
		})
	}
	if p.RxHeight != nil {
		values = append(values, &propsim_v1.ParameterValue{
			Name:  "rxheight",
			Value: &propsim_v1.ParameterValue_D{D: float64(*p.RxHeight)},
		})
	}
	txpower := float64(0)
	if p.Gain != nil {
		txpower = float64(*p.Gain)
	}
	values = append(values, &propsim_v1.ParameterValue{
		Name:  "txpower",
		Value: &propsim_v1.ParameterValue_D{D: txpower},
	})

	// Check ParameterValues a final time to ensure all required present.
	if err := CheckParameters(values, ps.Desc); err != nil {
		return nil, err
	}

	// Submit the job
	jobId := uuid.New()
	job := propsim_v1.Job{
		Id:         jobId.String(),
		Parameters: values,
	}
	req := propsim_v1.CreateJobRequest{Job: &job}
	log.Debug().Msg(fmt.Sprintf("propsim.CreateJob(%+v) parameter values: %+v", req, values))
	if resp, err := ps.client.CreateJob(ctx, &req); err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("propsim.CreateJob(%+v) error: %+v", req, err))
		return nil, err
	} else {
		log.Debug().Msg(fmt.Sprintf("propsim.CreateJob(%+v) success: %+v", req, resp))
		propsimJob := store.PropsimJob{
			Id:        uuid.New(),
			PropsimId: p.Id,
			ServiceId: *p.ServiceId,
			JobId:     &jobId,
			Status:    store.Created,
			Progress:  0.0,
		}

		return &propsimJob, nil
	}
}

func (pc *PropsimController) StartJob(ctx context.Context, propsimJob *store.PropsimJob) (err error) {
	var ps *PropsimService
	pc.mutex.RLock()
	if _ps, ok := pc.services[propsimJob.ServiceId.String()]; !ok {
		pc.mutex.RUnlock()
		return fmt.Errorf("unknown service")
	} else {
		ps = _ps
	}
	pc.mutex.RUnlock()

	var eventType int32
	req := propsim_v1.StartJobRequest{JobId: propsimJob.JobId.String()}
	var resp *propsim_v1.StartJobResponse
	if resp, err = ps.client.StartJob(ctx, &req); err != nil {
		msg := err.Error()
		log.Error().Err(err).Msg(fmt.Sprintf("propsim.StartJob(%+v) error: %s", req, msg))
		propsimJob.Status = store.Failed
		propsimJob.Message = &msg
		eventType = int32(event_v1.EventType_ET_FAILED)
	} else {
		log.Debug().Msg(fmt.Sprintf("propsim.StartJob(%+v) success: %+v", req, resp))
		propsimJob.Status = store.Scheduled
		propsimJob.Progress = 0.0
		eventType = int32(event_v1.EventType_ET_SCHEDULED)
	}
	pc.db.Save(propsimJob)

	// Generate an event.
	oid := propsimJob.Id.String()
	eh := subscription.EventHeader{
		Type:       eventType,
		Code:       int32(dst_v1.EventCode_EC_PROPSIM_JOB),
		SourceType: int32(event_v1.EventSourceType_EST_DST),
		SourceId:   pc.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       propsimJob.UpdatedAt,
	}
	e := subscription.Event{
		Header: eh,
		Object: propsimJob,
	}
	go pc.sm.Notify(&e)

	return err
}

func PropsimStatusFromProto(status propsim_v1.JobStatus) (store.PropsimStatus, error) {
	switch status {
	case propsim_v1.JobStatus_JS_CREATED:
		return store.Created, nil
	case propsim_v1.JobStatus_JS_SCHEDULED:
		return store.Scheduled, nil
	case propsim_v1.JobStatus_JS_RUNNING:
		return store.Running, nil
	case propsim_v1.JobStatus_JS_COMPLETE:
		return store.Complete, nil
	case propsim_v1.JobStatus_JS_FAILED:
		return store.Failed, nil
	case propsim_v1.JobStatus_JS_DELETED:
		return store.Deleted, nil
	default:
		return store.Unknown, fmt.Errorf("unknown propsim.JobStatus value (%v)", status)
	}
}

func JobStatusToEventType(status propsim_v1.JobStatus) event_v1.EventType {
	switch status {
	case propsim_v1.JobStatus_JS_CREATED:
		return event_v1.EventType_ET_CREATED
	case propsim_v1.JobStatus_JS_SCHEDULED:
		return event_v1.EventType_ET_SCHEDULED
	case propsim_v1.JobStatus_JS_RUNNING:
		return event_v1.EventType_ET_RUNNING
	case propsim_v1.JobStatus_JS_COMPLETE:
		return event_v1.EventType_ET_COMPLETED
	case propsim_v1.JobStatus_JS_FAILED:
		return event_v1.EventType_ET_FAILED
	case propsim_v1.JobStatus_JS_DELETED:
		return event_v1.EventType_ET_DELETED
	default:
		return event_v1.EventType_ET_OTHER
	}
}

func (pc *PropsimController) ProcessJobOutput(ctx context.Context, propsimJob *store.PropsimJob, jobOutput *propsim_v1.JobOutput) (*store.PropsimJobOutput, error) {
	m := jobOutput.GetMap()
	if m == nil || m.Type != propsim_v1.MapType_MT_GEOTIFF {
		log.Info().Msg(fmt.Sprintf("ProcessJobOutput: unsupported map type (%+v)", m))
		return nil, nil
	}
	var kind store.PropsimOutputKind
	var unit store.PropsimOutputUnit
	var perr error
	if kind, perr = store.PropsimOutputKindFromProto(m.Kind); perr != nil {
		log.Info().Msg(fmt.Sprintf("ProcessJobOutput: unsupported map kind (%+v)", m.Kind))
		return nil, nil
	} else if unit, perr = store.PropsimOutputUnitFromProto(m.Unit); perr != nil {
		log.Info().Msg(fmt.Sprintf("ProcessJobOutput: unsupported map unit (%+v)", m.Unit))
		return nil, nil
	}

	var jobOutputId *uuid.UUID
	if idParsed, err := uuid.Parse(jobOutput.Id); err == nil {
		jobOutputId = &idParsed
	}

	destDir := fmt.Sprintf("%s/propsims/%s/%s", pc.config.DataDir, propsimJob.PropsimId.String(), jobOutputId.String())
	destFile := destDir + "/map.tif"

	propsimJobOutput := store.PropsimJobOutput{
		Id:           uuid.New(),
		PropsimJobId: propsimJob.Id,
		JobOutputId:  jobOutputId,
		Type:         store.GeoTIFF,
		Kind:         kind,
		Unit:         unit,
		Name:         jobOutput.Name,
		Desc:         jobOutput.Desc,
		Path:         destFile,
	}

	if err := os.MkdirAll(destDir, 0750); err != nil {
		msg := fmt.Sprintf("ProcessJobOutput: failed to create data dir (%s, %v): %s", propsimJob.Id, destDir, err.Error())
		log.Error().Err(err).Msg(msg)
		propsimJobOutput.Message = &msg
		pc.db.Create(&propsimJobOutput)
		return &propsimJobOutput, err
	}

	var df *os.File

	if _df, err := os.Create(destFile); err != nil {
		msg := fmt.Sprintf("ProcessJobOutput: failed to create destFile (%s, %v): %s", propsimJob.Id, destFile, err.Error())
		log.Error().Err(err).Msg(msg)
		propsimJobOutput.Message = &msg
		pc.db.Create(&propsimJobOutput)
		return &propsimJobOutput, err
	} else {
		df = _df
	}

	var size uint64
	var data *[]byte
	if m.GetUrlData() != nil {
		u := m.GetUrlData()
		size = u.Size
		if resp, err := http.Get(u.Url); err != nil {
			msg := fmt.Sprintf("ProcessJobOutput: failed to fetch (%s, %v): %s", propsimJob.Id, u.Url, err.Error())
			log.Error().Err(err).Msg(msg)
			propsimJobOutput.Message = &msg
			pc.db.Create(&propsimJobOutput)
			return &propsimJobOutput, err
		} else if fdata, err := io.ReadAll(resp.Body); err != nil {
			//_, err := io.Copy(df, resp.Body); err != nil {
			msg := fmt.Sprintf("ProcessJobOutput: failed to read url body (%v): %s", propsimJob.Id, err.Error())
			log.Error().Err(err).Msg(msg)
			propsimJobOutput.Message = &msg
			pc.db.Create(&propsimJobOutput)
			return &propsimJobOutput, err
		} else if _, err := df.Write(fdata); err != nil {
			msg := fmt.Sprintf("ProcessJobOutput: failed to write destFile (%s, %v): %s", propsimJob.Id, destFile, err.Error())
			log.Error().Err(err).Msg(msg)
			propsimJobOutput.Message = &msg
			pc.db.Create(&propsimJobOutput)
			return &propsimJobOutput, err
		} else {
			resp.Body.Close()
			data = &fdata
		}
	} else if m.GetRawData() != nil {
		r := m.GetRawData()
		size = uint64(len(r.Data))
		if _, err := df.Write(r.Data); err != nil {
			msg := fmt.Sprintf("ProcessJobOutput: failed to write destFile (%s, %v): %s", propsimJob.Id, destFile, err.Error())
			log.Error().Err(err).Msg(msg)
			propsimJobOutput.Message = &msg
			pc.db.Create(&propsimJobOutput)
			return &propsimJobOutput, err
		} else {
			data = &r.Data
		}
	}
	df.Close()

	// If we have data, try to upload it as a raster into postgis, and into
	// geoserver, if that is configured.
	if data != nil {
		rasterId := -1
		res := pc.db.Raw("insert into propsim_rasters (rast) values (ST_FromGDALRaster(?::bytea)) returning rid", data).Scan(&rasterId)
		if res.Error != nil || res.RowsAffected != 1 {
			msg := fmt.Sprintf("ProcessJobOutput: failed to insert geotiff raster into postgis (%v): %v", propsimJob.Id, res.Error)
			log.Error().Err(res.Error).Msg(msg)
			propsimJobOutput.Message = &msg
			pc.db.Create(&propsimJobOutput)
			return &propsimJobOutput, fmt.Errorf(msg)
		} else {
			propsimJobOutput.PropsimRasterRid = &rasterId
		}

		supportedUnit := false
		if m.Unit == propsim_v1.MapUnit_MU_dB || m.Unit == propsim_v1.MapUnit_MU_dBm || m.Unit == propsim_v1.MapUnit_MU_LOS {
			supportedUnit = true
		}

		if supportedUnit && pc.config.GeoserverApi != "" && pc.config.GeoserverUser != "" && pc.config.GeoserverPassword != "" {
			gc := geoserver.NewGeoserverClient(pc.config.GeoserverApi, pc.config.GeoserverUser, pc.config.GeoserverPassword, true)
			layerName := "id_" + strings.ReplaceAll(propsimJobOutput.Id.String(), "-", "_")
			if _, _, err := gc.Import(pc.config.GeoserverWorkspace, bytes.NewReader(*data), layerName, pc.config.GeoserverDefaultStyle, pc.config.GeoserverSrsTransform, pc.config.GeoserverAnonymous); err != nil {
				msg := fmt.Sprintf("ProcessJobOutput: failed to insert geotiff raster into geoserver (%s): %v", propsimJob.Id, err)
				log.Error().Err(res.Error).Msg(msg)
				propsimJobOutput.Message = &msg
				pc.db.Create(&propsimJobOutput)
				return &propsimJobOutput, fmt.Errorf(msg)
			} else {
				propsimJobOutput.GeoserverWorkspace = &pc.config.GeoserverWorkspace
				propsimJobOutput.GeoserverLayer = &layerName
			}
		}
	}

	propsimJobOutput.Size = size
	pc.db.Create(&propsimJobOutput)

	return &propsimJobOutput, nil
}

func (pc *PropsimController) WatchJob(ctx context.Context, propsimJob *store.PropsimJob) error {
	var ps *PropsimService
	pc.mutex.RLock()
	if _ps, ok := pc.services[propsimJob.ServiceId.String()]; !ok {
		pc.mutex.RUnlock()
		return fmt.Errorf("unknown service")
	} else {
		ps = _ps
	}
	pc.mutex.RUnlock()

	req := propsim_v1.GetJobEventsRequest{JobId: propsimJob.JobId.String()}
	resp, err := ps.client.GetJobEvents(ctx, &req)
	if err != nil {
		log.Error().Err(err).Msg(fmt.Sprintf("propsim.GetJobEvents(%+v) error: %+v", req, err))
		return err
	}

	log.Debug().Msg(fmt.Sprintf("propsim.GetJobEvents(%+v) success: %+v", req, resp))

	for {
		var eventResp *propsim_v1.GetJobEventsResponse
		eventResp, err = resp.Recv()
		if err != nil {
			log.Error().Err(err).Msg(fmt.Sprintf("propsim.GetJobEvents(%s) error: %s", propsimJob.JobId.String(), err.Error()))
			return err
		} else if eventResp.Job == nil {
			log.Warn().Msg(fmt.Sprintf("propsim.GetJobEvents(%s): no job, skipping (%+v)", propsimJob.JobId.String(), eventResp))
			continue
		} else if eventResp.Job.Status == nil {
			log.Warn().Msg(fmt.Sprintf("propsim.GetJobEvents(%s): no status, skipping (%+v)", propsimJob.JobId.String(), eventResp))
			continue
		}

		job := eventResp.Job
		if newStatus, err := PropsimStatusFromProto(*job.Status); err != nil {
			log.Warn().Msg(fmt.Sprintf("propsim.GetJobEvents(%s) error: %s", propsimJob.JobId.String(), err.Error()))
			continue
		} else {
			propsimJob.Status = newStatus
			if job.CompletionPercentage != nil {
				propsimJob.Progress = float64(*job.CompletionPercentage)
			}
		}
		pc.db.Save(propsimJob)

		var eventType int32
		if eventResp.Event == propsim_v1.JobEvent_JOB_EVENT_PROGRESS {
			eventType = int32(event_v1.EventType_ET_PROGRESS)
		} else if eventResp.Event == propsim_v1.JobEvent_JOB_EVENT_OUTPUT {
			eventType = int32(event_v1.EventType_ET_UPDATED)
		} else {
			eventType = int32(event_v1.EventType_ET_UPDATED)
		}

		// For now, only grab all outputs on COMPLETE, not necessarily as
		// they roll in.  Only grab geotiff outputs for now.
		if *job.Status == propsim_v1.JobStatus_JS_COMPLETE {
			if job.Outputs != nil {
				for _, jobOutput := range job.Outputs {
					propsimJobOutput, err := pc.ProcessJobOutput(ctx, propsimJob, jobOutput)
					if err != nil {
						// Move propsimJob into the failed state:
						propsimJob.Status = store.Failed
						if propsimJobOutput != nil {
							propsimJob.Message = propsimJobOutput.Message
						}
					}

				}
			}
			if propsimJob.Status != store.Failed {
				propsimJob.Status = store.Complete
			}
			t := time.Now().UTC()
			propsimJob.FinishedAt = &t
		} else if *job.Status == propsim_v1.JobStatus_JS_FAILED {
			propsimJob.Status = store.Failed
			t := time.Now().UTC()
			propsimJob.FinishedAt = &t
		}

		pc.db.Save(&propsimJob)

		// Generate an event.
		oid := propsimJob.Id.String()
		eh := subscription.EventHeader{
			Type:       eventType,
			Code:       int32(dst_v1.EventCode_EC_PROPSIM_JOB),
			SourceType: int32(event_v1.EventSourceType_EST_DST),
			SourceId:   pc.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       propsimJob.UpdatedAt,
		}
		e := subscription.Event{
			Header: eh,
			Object: propsimJob,
		}
		go pc.sm.Notify(&e)

		// If the job finished, stop monitoring it.
		if propsimJob.FinishedAt != nil {
			break
		}
	}

	return nil
}

func (pc *PropsimController) RunPropsim(ctx context.Context, p *store.Propsim) error {
	// Place the propsim into the running state.
	t := time.Now().UTC()
	p.UpdatedAt = &t
	p.Status = store.Running
	pc.db.Save(p)

	// Generate an event.
	eventType := int32(event_v1.EventType_ET_RUNNING)
	oid := p.Id.String()
	eh := subscription.EventHeader{
		Type:       eventType,
		Code:       int32(dst_v1.EventCode_EC_PROPSIM),
		SourceType: int32(event_v1.EventSourceType_EST_DST),
		SourceId:   pc.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
	}
	e := subscription.Event{
		Header: eh,
		Object: p,
	}
	go pc.sm.Notify(&e)

	// Run all jobs.
	var savedErr *error
	for _, propsimJob := range p.Jobs {
		jobCtx, cancel := context.WithTimeout(context.Background(), time.Second*10)
		defer cancel()
		if err := pc.StartJob(jobCtx, &propsimJob); err != nil {
			log.Error().Err(err).Msg(fmt.Sprintf("RunPropsim: error: %s", err.Error()))
			savedErr = &err
		}
		cancel()

		// Update the job, send an event.
		t = time.Now().UTC()
		if savedErr != nil {
			propsimJob.Status = store.Failed
			msg := (*savedErr).Error()
			propsimJob.Message = &msg
			eventType = int32(event_v1.EventType_ET_FAILED)
			propsimJob.FinishedAt = &t
		} else {
			propsimJob.Status = store.Running
			eventType = int32(event_v1.EventType_ET_RUNNING)
			propsimJob.UpdatedAt = &t
		}
		pc.db.Save(p)
		// Generate an event.
		oid = propsimJob.Id.String()
		eh = subscription.EventHeader{
			Type:       eventType,
			Code:       int32(dst_v1.EventCode_EC_PROPSIM_JOB),
			SourceType: int32(event_v1.EventSourceType_EST_DST),
			SourceId:   pc.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       &t,
		}
		e := subscription.Event{
			Header: eh,
			Object: propsimJob,
		}
		go pc.sm.Notify(&e)

		// If StartJob failed, fail the entire propsim.
		if savedErr != nil {
			break
		}

		jobCtx = context.Background()
		if err := pc.WatchJob(jobCtx, &propsimJob); err != nil {
			log.Error().Err(err).Msg(fmt.Sprintf("RunPropsim: error: %s", err.Error()))
			savedErr = &err
		}

		// Update the job, send an event.
		t = time.Now().UTC()
		if savedErr != nil {
			propsimJob.Status = store.Failed
			msg := (*savedErr).Error()
			propsimJob.Message = &msg
			eventType = int32(event_v1.EventType_ET_FAILED)
		} else {
			propsimJob.Status = store.Complete
			eventType = int32(event_v1.EventType_ET_COMPLETED)
		}
		propsimJob.FinishedAt = &t
		pc.db.Save(p)
		// Generate an event.
		oid = propsimJob.Id.String()
		eh = subscription.EventHeader{
			Type:       eventType,
			Code:       int32(dst_v1.EventCode_EC_PROPSIM_JOB),
			SourceType: int32(event_v1.EventSourceType_EST_DST),
			SourceId:   pc.config.ServiceId,
			Id:         uuid.New().String(),
			ObjectId:   &oid,
			Time:       &t,
		}
		e = subscription.Event{
			Header: eh,
			Object: propsimJob,
		}
		go pc.sm.Notify(&e)

		// If StartJob failed, fail the entire propsim.
		if savedErr != nil {
			break
		}
	}

	// Update the propsim.
	t = time.Now().UTC()
	p.FinishedAt = &t
	if savedErr != nil {
		p.Status = store.Failed
		eventType = int32(event_v1.EventType_ET_FAILED)
	} else {
		p.Status = store.Complete
		eventType = int32(event_v1.EventType_ET_COMPLETED)
	}
	pc.db.Save(p)

	// Generate an event.
	oid = p.Id.String()
	eh = subscription.EventHeader{
		Type:       eventType,
		Code:       int32(dst_v1.EventCode_EC_PROPSIM),
		SourceType: int32(event_v1.EventSourceType_EST_DST),
		SourceId:   pc.config.ServiceId,
		Id:         uuid.New().String(),
		ObjectId:   &oid,
		Time:       &t,
	}
	e = subscription.Event{
		Header: eh,
		Object: p,
	}
	go pc.sm.Notify(&e)

	if savedErr != nil {
		return *savedErr
	} else {
		return nil
	}
}

func (pc *PropsimController) Run() error {
	return nil
}

func (pc *PropsimController) Shutdown() error {
	return nil
}
