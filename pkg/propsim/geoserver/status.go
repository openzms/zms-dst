// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package geoserver

import (
	"encoding/json"
	"fmt"
	"io/ioutil"

	"github.com/rs/zerolog/log"
)

type SystemStatusMetricValue struct {
	Available   bool   `json:"available"`
	Category    string `json:"category"`
	Description string `json:"description"`
	Identifier  string `json:"identifier"`
	Name        string `json:"name"`
	Priority    int    `json:"int"`
	Unit        string `json:"unit"`
	Value       string `json:"value"`
}

type SystemStatusMetrics struct {
	Metrics []SystemStatusMetricValue `json:"metric"`
}

type SystemStatus struct {
	Metrics SystemStatusMetrics `json:"metrics"`
}

func (gc *GeoserverClient) GetSystemStatus() (*SystemStatus, error) {
	var ret *SystemStatus
	if resp, err := gc.GET("/about/system-status"); err != nil {
		return nil, err
	} else if body, err := ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	} else {
		ret = new(SystemStatus)
		if err := json.Unmarshal(body, ret); err != nil {
			return nil, err
		} else {
			log.Debug().Msg(fmt.Sprintf("GetSystemStatus: ret = %+v", ret))
		}
	}
	return ret, nil
}
