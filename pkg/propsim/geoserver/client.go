// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package geoserver

import (
	"crypto/tls"
	"errors"
	"fmt"
	"io"
	"net/http"

	"github.com/rs/zerolog/log"
)

type GeoserverClient struct {
	BaseApiUrl string
	Username   string
	Password   string
	Client     *http.Client
}

type HttpError interface {
	error
	Code() int
}

type GeoserverClientError struct {
	err  error
	code int
}

func NewGeoserverClientError(err error, code int) GeoserverClientError {
	return GeoserverClientError{err: err, code: code}
}

func (e GeoserverClientError) Error() string {
	return e.err.Error()
}

func (e GeoserverClientError) Code() int {
	return e.code
}

func NewGeoserverClient(baseApiUrl string, username string, password string, insecureTls bool) (client *GeoserverClient) {
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: insecureTls},
	}
	httpClient := &http.Client{Transport: tr}

	client = &GeoserverClient{
		BaseApiUrl: baseApiUrl,
		Username:   username,
		Password:   password,
		Client:     httpClient,
	}

	return client
}

func (client *GeoserverClient) GET(path string) (*http.Response, error) {
	req, err := http.NewRequest(
		"GET", client.BaseApiUrl+path, nil)
	req.SetBasicAuth(client.Username, client.Password)
	req.Header.Set("Accept", "application/json")
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.GET(%s): req = %+v", path, req))

	var resp *http.Response
	if resp, err = client.Client.Do(req); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.GET(%s): resp = %+v", path, resp))
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return nil, NewGeoserverClientError(errors.New(resp.Status), resp.StatusCode)
	}

	return resp, nil
}

func (client *GeoserverClient) POST(path string, reader io.Reader, headers map[string]string) (*http.Response, error) {
	req, err := http.NewRequest(
		"POST", client.BaseApiUrl+path, reader)
	req.SetBasicAuth(client.Username, client.Password)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-type", "application/json")
	if headers != nil && len(headers) > 0 {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.POST(%s)", path))

	var resp *http.Response
	if resp, err = client.Client.Do(req); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.POST(%s): resp = %+v", path, resp))
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return nil, NewGeoserverClientError(errors.New(resp.Status), resp.StatusCode)
	}

	return resp, nil
}

func (client *GeoserverClient) PUT(path string, reader io.Reader, headers map[string]string) (*http.Response, error) {
	req, err := http.NewRequest(
		"PUT", client.BaseApiUrl+path, reader)
	req.SetBasicAuth(client.Username, client.Password)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Content-type", "application/json")
	if headers != nil && len(headers) > 0 {
		for k, v := range headers {
			req.Header.Set(k, v)
		}
	}
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.PUT(%s)", path))

	var resp *http.Response
	if resp, err = client.Client.Do(req); err != nil {
		return nil, err
	}
	log.Debug().Msg(fmt.Sprintf("GeoserverClient.PUT(%s): resp = %+v", path, resp))
	if resp.StatusCode < 200 || resp.StatusCode >= 400 {
		return nil, NewGeoserverClientError(errors.New(resp.Status), resp.StatusCode)
	}

	return resp, nil
}
