// SPDX-FileCopyrightText: 2024-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package geoserver

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"mime/multipart"
	"time"

	"github.com/rs/zerolog/log"
)

type WorkspaceInfo struct {
	Name         *string `json:"name,omitempty"`
	Isolated     *bool   `json:"isolated,omitempty"`
	DateCreated  *string `json:"dateCreated,omitempty"`
	DateModified *string `json:"dateModified,omitempty"`
}

type WorkspaceWrapper struct {
	Workspace *WorkspaceInfo `json:"workspace,omitempty"`
}

type ImportInfo struct {
	Id              *int              `json:"id,omitempty"`
	Href            *string           `json:"href,omitempty"`
	State           *string           `json:"state,omitempty"`
	Archive         *bool             `json:"archive,omitempty"`
	TargetWorkspace *WorkspaceWrapper `json:"targetWorkspace,omitempty"`
	Tasks           *[]ImportTask     `json:"tasks,omitempty"`
}

type Import struct {
	Import ImportInfo `json:"import"`
}

type CoverageStoreInfo struct {
	Name *string `json:"name,omitempty"`
	Type *string `json:"type,omitempty"`
}

type ImportTaskTarget struct {
	Href          *string            `json:"href,omitempty"`
	CoverageStore *CoverageStoreInfo `json:"coverageStore,omitempty"`
}

type ImportTaskData struct {
	Type   *string `json:"type,omitempty"`
	Format *string `json:"format,omitempty"`
	File   *string `json:"file,omitempty"`
}

type ImportTaskLayer struct {
	Name *string `json:"name,omitempty"`
	Href *string `json:"href,omitempty"`
}

type ImportTaskTransformInfo struct {
	Type    *string   `json:"type,omitempty"`
	Options *[]string `json:"options,omitempty"`
}

type ImportTaskTransform struct {
	Type       *string                    `json:"type,omitempty"`
	Transforms *[]ImportTaskTransformInfo `json:"transforms,omitempty"`
}

type ImportTaskInfo struct {
	Id             *int                 `json:"id,omitempty"`
	Href           *string              `json:"href,omitempty"`
	State          *string              `json:"state,omitempty"`
	UpdateMode     *string              `json:"updateMode,omitempty"`
	Data           *ImportTaskData      `json:"data,omitempty"`
	Target         *ImportTaskTarget    `json:"target,omitempty"`
	Progress       *string              `json:"progress,omitempty"`
	Layer          *ImportTaskLayer     `json:"layer,omitempty"`
	TransformChain *ImportTaskTransform `json:"transformChain,omitempty"`
}

type ImportTask struct {
	Task ImportTaskInfo `json:"task"`
}

func (gc *GeoserverClient) CreateImport(i *Import) (*Import, error) {
	var ret *Import
	if j, err := json.Marshal(i); err != nil {
		return nil, err
	} else if resp, err := gc.POST("/imports", bytes.NewReader(j), nil); err != nil {
		return nil, err
	} else if body, err := ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	} else {
		ret = new(Import)
		if err := json.Unmarshal(body, ret); err != nil {
			return nil, err
		} else {
			log.Debug().Msg(fmt.Sprintf("CreateImport: ret = %+v", ret))
		}
	}
	return ret, nil
}

type Resource struct {
	Class *string `json:"@clss,omitempty"`
	Name  *string `json:"name,omitempty"`
	Href  *string `json:"href,omitempty"`
}

type StyleInfo struct {
	Name *string `json:"name,omitempty"`
	Href *string `json:"href,omitempty"`
}

type LayerInfo struct {
	Name         *string    `json:"name,omitempty"`
	Type         *string    `json:"type,omitempty"`
	DateCreated  *string    `json:"dateCreated,omitempty"`
	DateModified *string    `json:"dateModified,omitempty"`
	DefaultStyle *StyleInfo `json:"defaultStyle",omitempty"`
}

type Layer struct {
	Layer LayerInfo `json:"layer"`
}

func (gc *GeoserverClient) CreateImportTask(importId int, filedata io.Reader, filename string) (it *ImportTask, err error) {
	var buf bytes.Buffer
	mpw := multipart.NewWriter(&buf)
	var w io.Writer
	if w, err = mpw.CreateFormFile("filedata", filename); err != nil {
		return nil, err
	}
	if _, err = io.Copy(w, filedata); err != nil {
		return nil, err
	}
	mpw.Close()

	u := fmt.Sprintf("/imports/%d/tasks", importId)
	headers := map[string]string{"Content-Type": mpw.FormDataContentType()}
	if resp, err := gc.POST(u, &buf, headers); err != nil {
		return nil, err
	} else if body, err := ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	} else {
		it = new(ImportTask)
		if err := json.Unmarshal(body, it); err != nil {
			return nil, err
		} else {
			log.Debug().Msg(fmt.Sprintf("CreateImportTask: ret Id = %+v", it.Task.Id))
		}
	}

	return it, nil
}

func (gc *GeoserverClient) CreateImportTaskTransform(importId int, importTaskId int, itt *ImportTaskTransformInfo) error {
	u := fmt.Sprintf("/imports/%d/tasks/%d/transforms", importId, importTaskId)
	if j, err := json.Marshal(itt); err != nil {
		return err
	} else if _, err := gc.POST(u, bytes.NewReader(j), nil); err != nil {
		return err
	}
	return nil
}

func (gc *GeoserverClient) FinalizeImport(importId int) error {
	u := fmt.Sprintf("/imports/%d", importId)
	if _, err := gc.POST(u, nil, nil); err != nil {
		return err
	}
	return nil
}

func (gc *GeoserverClient) GetLayer(fqLayer string) (*Layer, error) {
	var ret *Layer
	u := "/layers/" + fqLayer
	if resp, err := gc.GET(u); err != nil {
		return nil, err
	} else if body, err := ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	} else {
		ret = new(Layer)
		if err := json.Unmarshal(body, ret); err != nil {
			return nil, err
		} else {
			log.Debug().Msg(fmt.Sprintf("GetLayer: ret = %+v", ret))
		}
	}
	return ret, nil
}

func (gc *GeoserverClient) UpdateLayer(layer *Layer, fqLayer string) error {
	u := "/layers/" + fqLayer
	if j, err := json.Marshal(layer); err != nil {
		return err
	} else if _, err := gc.PUT(u, bytes.NewReader(j), nil); err != nil {
		return err
	} else {
		return nil
	}
}

func (gc *GeoserverClient) AddLayerAnonymousPolicy(aclFqLayer string) error {
	u := "/security/acl/layers"
	j := "{\"" + aclFqLayer + ".r\":\"ROLE_ANONYMOUS\"}"
	if _, err := gc.POST(u, bytes.NewReader([]byte(j)), nil); err != nil {
		return err
	} else {
		return nil
	}
}

func (gc *GeoserverClient) Import(workspace string, filedata io.Reader, layer string, style string, srsTransform string, anonymous bool) (*Layer, *Import, error) {
	iimp := Import{
		Import: ImportInfo{
			TargetWorkspace: &WorkspaceWrapper{
				Workspace: &WorkspaceInfo{
					Name: &workspace,
				},
			},
		},
	}
	var oImport *Import
	if imp, err := gc.CreateImport(&iimp); err != nil {
		return nil, nil, err
	} else {
		oImport = imp
	}
	importId := *oImport.Import.Id
	log.Debug().Msg(fmt.Sprintf("Import: created (%d, %+v)", importId, oImport))

	var oImportTask *ImportTask
	if task, err := gc.CreateImportTask(importId, filedata, layer+".tif"); err != nil {
		return nil, nil, err
	} else {
		oImportTask = task
	}
	importTaskId := *oImportTask.Task.Id
	log.Debug().Msg(fmt.Sprintf("Import: created task (%d, %+v)", importTaskId, oImportTask))

	if srsTransform != "" {
		ittType := "GdalWarpTransform"
		ittOptions := []string{"-t_srs", srsTransform}
		importTaskTransform := ImportTaskTransformInfo{
			Type:    &ittType,
			Options: &ittOptions,
		}
		if err := gc.CreateImportTaskTransform(importId, importTaskId, &importTaskTransform); err != nil {
			return nil, nil, err
		}
		log.Debug().Msg(fmt.Sprintf("Import: created task transform (%+v)", importTaskTransform))
	}

	if err := gc.FinalizeImport(importId); err != nil {
		return nil, nil, err
	}

	// Wait for layer to appear.
	time.Sleep(4 * time.Second)
	var oLayer *Layer
	fqLayer := workspace + ":" + layer
	for {
		if layer, err := gc.GetLayer(fqLayer); err != nil {
			log.Debug().Msg(fmt.Sprintf("Import: awaiting imported layer %s", fqLayer))
			time.Sleep(1 * time.Second)
		} else {
			oLayer = layer
			break
		}
	}

	if style != "" {
		oLayer.Layer.DefaultStyle.Name = &style
		if err := gc.UpdateLayer(oLayer, fqLayer); err != nil {
			return oLayer, oImport, err
		}
	}

	if anonymous {
		aclFqLayer := workspace + "." + layer
		if err := gc.AddLayerAnonymousPolicy(aclFqLayer); err != nil {
			return oLayer, oImport, err
		}
	}

	return oLayer, oImport, nil
}
