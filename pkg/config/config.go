// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package config

import (
	"flag"
	"os"
	"strconv"
)

type Config struct {
	Verbose             bool
	Debug               bool
	HttpEndpointListen  string
	HttpEndpoint        string
	HealthHttpEndpoint  string
	RpcEndpointListen   string
	RpcEndpoint         string
	ServiceId           string
	ServiceName         string
	DbDriver            string
	DbDsn               string
	DbAutoMigrate       bool
	IdentityRpcEndpoint string
	DataDir             string
	GeoserverApi        string
	GeoserverUser       string
	GeoserverPassword   string
	GeoserverWorkspace  string
	GeoserverSrsTransform string
	GeoserverDefaultStyle string
	GeoserverAnonymous  bool
}

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

func LoadConfig() *Config {
	var config Config
	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")

	flag.StringVar(&config.HttpEndpointListen, "http-endpoint-listen",
		getEnvString("HTTP_ENDPOINT_LISTEN", "0.0.0.0:8020"), "HTTP endpoint bind address")
	flag.StringVar(&config.HttpEndpoint, "http-endpoint",
		getEnvString("HTTP_ENDPOINT", "0.0.0.0:8020"), "HTTP endpoint address")
	flag.StringVar(&config.HealthHttpEndpoint, "health-http-endpoint",
		getEnvString("HEALTH_HTTP_ENDPOINT", "0.0.0.0:8021"), "HTTP health endpoint address")
	flag.StringVar(&config.RpcEndpointListen, "rpc-endpoint-listen",
		getEnvString("RPC_ENDPOINT_LISTEN", "0.0.0.0:8022"), "RPC endpoint bind address")
	flag.StringVar(&config.RpcEndpoint, "rpc-endpoint",
		getEnvString("RPC_ENDPOINT", "0.0.0.0:8022"), "RPC endpoint address")
	flag.StringVar(&config.ServiceId, "service-id",
		getEnvString("SERVICE_ID", "33453677-4573-4b4a-b006-8fd073220003"), "Service ID")
	flag.StringVar(&config.ServiceName, "service-name",
		getEnvString("SERVICE_NAME", "dst"), "Service Name")
	flag.StringVar(&config.DbDriver, "db-driver",
		getEnvString("DB_DRIVER", "sqlite"), "Database driver (sqlite, postgres)")
	flag.StringVar(&config.DbDsn, "db-dsn",
		getEnvString("DB_DSN", "file::memory:?cache=shared"), "Database DSN")
	flag.BoolVar(&config.DbAutoMigrate, "db-auto-migrate",
		getEnvBool("DB_AUTO_MIGRATE", true), "Enable/disable automatic database migration")
	flag.StringVar(&config.IdentityRpcEndpoint, "identity-rpc-endpoint",
		getEnvString("IDENTITY_RPC_ENDPOINT", "0.0.0.0:8002"), "Identity service RPC endpoint address")
	flag.StringVar(&config.DataDir, "data-dir",
		getEnvString("DATA_DIR", "/data"), "Persistent storage for observation data")
	flag.StringVar(&config.GeoserverApi, "geoserver-api",
		getEnvString("GEOSERVER_API", ""), "A Geoserver instance REST API URL where propagation simulations can be stored (e.g. http://zms-dst-geoserver-local-dev:8080/geoserver/rest)")
	flag.StringVar(&config.GeoserverUser, "geoserver-user",
		getEnvString("GEOSERVER_USER", "admin"), "A Geoserver username.")
	flag.StringVar(&config.GeoserverPassword, "geoserver-password",
		getEnvString("GEOSERVER_PASSWORD", ""), "The password for the geoserver user account.")
	flag.StringVar(&config.GeoserverWorkspace, "geoserver-workspace",
		getEnvString("GEOSERVER_WORKSPACE", "zms"), "The target Geoserver workspace.")
	flag.StringVar(&config.GeoserverSrsTransform, "geoserver-srs-transform",
		getEnvString("GEOSERVER_SRS_TRANSFORM", ""), "Transform Geoserver layer imports to a new coordinate reference system; e.g. EPSG:3857 .")
	flag.StringVar(&config.GeoserverDefaultStyle, "geoserver-default-style",
		getEnvString("GEOSERVER_DEFAULT_STYLE", ""), "The default Geoserver style to apply to imported layers.")
	flag.BoolVar(&config.GeoserverAnonymous, "geoserver-anonymous",
		getEnvBool("GEOSERVER_ANONYMOUS", false), "If true, set all imported Geoserver layers to anonymous read")

	flag.Parse()

	return &config
}
