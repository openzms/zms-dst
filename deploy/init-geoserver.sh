#!/bin/sh

set -x
set -e

GEOSERVER_USER=admin
GEOSERVER_DEFAULT_PASS=geoserver

if [ -z $GEOSERVER_API ]; then
    echo "ERROR: env var GEOSERVER_API unspecified; aborting"
    exit 1
fi

if [ -z $GEOSERVER_PASS ]; then
    echo "ERROR: env var GEOSERVER_PASS unspecified; aborting"
    exit 1
fi

curl --fail-with-body -H "Content-type: application/json" \
    -i -X GET -u $GEOSERVER_USER:$GEOSERVER_DEFAULT_PASS \
    $GEOSERVER_API/about/system-status

curl --fail-with-body -H "Content-type: application/json" \
    -i -X PUT -u $GEOSERVER_USER:$GEOSERVER_DEFAULT_PASS \
    -d '{"newPassword":"'$GEOSERVER_PASS'"}' \
    $GEOSERVER_API/security/self/password

sleep 8

curl --fail-with-body -H "Content-type: application/json" \
    -i -X GET -u $GEOSERVER_USER:$GEOSERVER_PASS \
    $GEOSERVER_API/about/system-status

curl --fail-with-body -H "Content-type: application/json" \
    -i -X POST -u $GEOSERVER_USER:$GEOSERVER_PASS \
    -d '{"workspace":{"name":"zms"}}' \
    $GEOSERVER_API/workspaces\?default=true

curl --fail-with-body -H "Content-type: application/vnd.ogc.sld+xml" \
    -i -X POST -u $GEOSERVER_USER:$GEOSERVER_PASS \
    -d@zms_qgis_viridis.sld \
    $GEOSERVER_API/styles

WORKSPACES_REMOVE="cite it.geosolutions nurc sde sf tiger topp"
set +e
for ws in $WORKSPACES_REMOVE ; do
    curl --fail-with-body -H "Content-type: application/json" \
        -i -X DELETE -u $GEOSERVER_USER:$GEOSERVER_PASS \
        $GEOSERVER_API/workspaces/$ws\?recurse=true
    sleep 2
done
set -e
