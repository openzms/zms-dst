<?xml version="1.0" encoding="UTF-8"?>
<StyledLayerDescriptor xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc" xmlns:sld="http://www.opengis.net/sld" version="1.0.0" xmlns:gml="http://www.opengis.net/gml">
  <UserLayer>
    <sld:LayerFeatureConstraints>
      <sld:FeatureTypeConstraint/>
    </sld:LayerFeatureConstraints>
    <sld:UserStyle>
      <sld:Name>zms_qgis_viridis</sld:Name>
      <sld:FeatureTypeStyle>
        <sld:Rule>
          <sld:RasterSymbolizer>
            <sld:ChannelSelection>
              <sld:GrayChannel>
                <sld:SourceChannelName>1</sld:SourceChannelName>
              </sld:GrayChannel>
            </sld:ChannelSelection>
            <sld:ColorMap type="ramp">
              <sld:ColorMapEntry color="#440154" quantity="-200" label="-200.0000"/>
              <sld:ColorMapEntry color="#46085c" quantity="-198.235298" label="-198.2353"/>
              <sld:ColorMapEntry color="#471063" quantity="-196.47058699999999" label="-196.4706"/>
              <sld:ColorMapEntry color="#481769" quantity="-194.70588499999999" label="-194.7059"/>
              <sld:ColorMapEntry color="#481d6f" quantity="-192.94117399999999" label="-192.9412"/>
              <sld:ColorMapEntry color="#482475" quantity="-191.17647199999999" label="-191.1765"/>
              <sld:ColorMapEntry color="#472a7a" quantity="-189.41176999999999" label="-189.4118"/>
              <sld:ColorMapEntry color="#46307e" quantity="-187.64705000000001" label="-187.6471"/>
              <sld:ColorMapEntry color="#453781" quantity="-185.88233" label="-185.8823"/>
              <sld:ColorMapEntry color="#433d84" quantity="-184.11761000000001" label="-184.1176"/>
              <sld:ColorMapEntry color="#414287" quantity="-182.35298" label="-182.3530"/>
              <sld:ColorMapEntry color="#3f4889" quantity="-180.58825999999999" label="-180.5883"/>
              <sld:ColorMapEntry color="#3d4e8a" quantity="-178.82354000000001" label="-178.8235"/>
              <sld:ColorMapEntry color="#3a538b" quantity="-177.05882" label="-177.0588"/>
              <sld:ColorMapEntry color="#38598c" quantity="-175.29410000000001" label="-175.2941"/>
              <sld:ColorMapEntry color="#355e8d" quantity="-173.52938" label="-173.5294"/>
              <sld:ColorMapEntry color="#33638d" quantity="-171.76474999999999" label="-171.7647"/>
              <sld:ColorMapEntry color="#31688e" quantity="-170.00003000000001" label="-170.0000"/>
              <sld:ColorMapEntry color="#2e6d8e" quantity="-168.23531" label="-168.2353"/>
              <sld:ColorMapEntry color="#2c718e" quantity="-166.47058999999999" label="-166.4706"/>
              <sld:ColorMapEntry color="#2a768e" quantity="-164.70587" label="-164.7059"/>
              <sld:ColorMapEntry color="#297b8e" quantity="-162.94114999999999" label="-162.9411"/>
              <sld:ColorMapEntry color="#27808e" quantity="-161.17643000000001" label="-161.1764"/>
              <sld:ColorMapEntry color="#25848e" quantity="-159.4118" label="-159.4118"/>
              <sld:ColorMapEntry color="#23898e" quantity="-157.64707999999999" label="-157.6471"/>
              <sld:ColorMapEntry color="#218e8d" quantity="-155.88236000000001" label="-155.8824"/>
              <sld:ColorMapEntry color="#20928c" quantity="-154.11763999999999" label="-154.1176"/>
              <sld:ColorMapEntry color="#1f978b" quantity="-152.35292000000001" label="-152.3529"/>
              <sld:ColorMapEntry color="#1e9c89" quantity="-150.5882" label="-150.5882"/>
              <sld:ColorMapEntry color="#1fa188" quantity="-148.82356999999999" label="-148.8236"/>
              <sld:ColorMapEntry color="#21a585" quantity="-147.05885000000001" label="-147.0589"/>
              <sld:ColorMapEntry color="#24aa83" quantity="-145.29413" label="-145.2941"/>
              <sld:ColorMapEntry color="#28ae80" quantity="-143.52941000000001" label="-143.5294"/>
              <sld:ColorMapEntry color="#2eb37c" quantity="-141.76469" label="-141.7647"/>
              <sld:ColorMapEntry color="#35b779" quantity="-139.99996999999999" label="-140.0000"/>
              <sld:ColorMapEntry color="#3dbc74" quantity="-138.23525000000001" label="-138.2353"/>
              <sld:ColorMapEntry color="#46c06f" quantity="-136.47062" label="-136.4706"/>
              <sld:ColorMapEntry color="#50c46a" quantity="-134.70590000000001" label="-134.7059"/>
              <sld:ColorMapEntry color="#5ac864" quantity="-132.94118" label="-132.9412"/>
              <sld:ColorMapEntry color="#65cb5e" quantity="-131.17645999999999" label="-131.1765"/>
              <sld:ColorMapEntry color="#70cf57" quantity="-129.41174000000001" label="-129.4117"/>
              <sld:ColorMapEntry color="#7cd250" quantity="-127.64702" label="-127.6470"/>
              <sld:ColorMapEntry color="#89d548" quantity="-125.88239" label="-125.8824"/>
              <sld:ColorMapEntry color="#95d840" quantity="-124.11767" label="-124.1177"/>
              <sld:ColorMapEntry color="#a2da37" quantity="-122.35295000000001" label="-122.3530"/>
              <sld:ColorMapEntry color="#b0dd2f" quantity="-120.58823" label="-120.5882"/>
              <sld:ColorMapEntry color="#bddf26" quantity="-118.82351" label="-118.8235"/>
              <sld:ColorMapEntry color="#cae11f" quantity="-117.05879" label="-117.0588"/>
              <sld:ColorMapEntry color="#d8e219" quantity="-115.29416000000001" label="-115.2942"/>
              <sld:ColorMapEntry color="#e5e419" quantity="-113.52944000000001" label="-113.5294"/>
              <sld:ColorMapEntry color="#f1e51d" quantity="-111.76472" label="-111.7647"/>
              <sld:ColorMapEntry color="#fde725" quantity="-110" label="-110.0000"/>
            </sld:ColorMap>
          </sld:RasterSymbolizer>
        </sld:Rule>
      </sld:FeatureTypeStyle>
    </sld:UserStyle>
  </UserLayer>
</StyledLayerDescriptor>
