// SPDX-FileCopyrightText: 2023-present University of Utah <info@openzms.org>
//
// SPDX-License-Identifier: Apache-2.0

package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"os"
	"strconv"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	"gitlab.flux.utah.edu/openzms/zms-dst/pkg/propsim/geoserver"
)

func getEnvBool(name string, defValue bool) bool {
	if val, ok := os.LookupEnv(name); ok {
		if pval, err := strconv.ParseBool(val); err != nil {
			pval = false
		} else {
			return pval
		}
	}
	return defValue
}

func getEnvString(name string, defValue string) string {
	if val, ok := os.LookupEnv(name); ok {
		return val
	}
	return defValue
}

type Config struct {
	Debug        bool
	Verbose      bool
	BaseUrl      string
	User         string
	Password     string
	Workspace    string
	Filename     string
	Layer        string
	Style        string
	SrsTransform string
	Anonymous    bool
}

func main() {
	config := Config{}

	flag.BoolVar(&config.Debug, "debug",
		getEnvBool("LOG_DEBUG", false), "Enable debug logging.")
	flag.BoolVar(&config.Verbose, "verbose",
		getEnvBool("LOG_VERBOSE", false), "Enable verbose logging.")
	flag.StringVar(&config.BaseUrl, "base-url",
		getEnvString("BASE_URL", "http://localhost:8080/geoserver/rest"), "Full Geoserver base url.")
	flag.StringVar(&config.User, "user",
		getEnvString("USER", "admin"), "Geoserver username.")
	flag.StringVar(&config.Password, "password",
		getEnvString("PASSWORD", "geoserver"), "Geoserver password.")
	flag.StringVar(&config.Workspace, "workspace",
		getEnvString("WORKSPACE", "zms"), "Target workspace within Geoserver.")
	flag.StringVar(&config.Filename, "filename",
		getEnvString("FILENAME", ""), "Filename to import into Geoserver.")
	flag.StringVar(&config.Layer, "layer",
		getEnvString("LAYER", ""), "A name to use for the imported layer.")
	flag.StringVar(&config.Style, "style",
		getEnvString("STYLE", ""), "An existing style to apply as the layer default style.")
	flag.StringVar(&config.SrsTransform, "srs-transform",
		getEnvString("SRS_TRANSFORM", ""), "A target coordinate reference system, e.g. EPSG:3857 .")
	flag.BoolVar(&config.Anonymous, "anonymous",
		getEnvBool("ANONYMOUS", false), "Set true if layer should be accessible by anyone.")
	flag.Parse()

	if config.Debug {
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	} else if config.Verbose {
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	} else {
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	}

	args := flag.Args()
	if len(args) == 0 {
		log.Fatal().Msg("no subcommand selected")
	}

	client := geoserver.NewGeoserverClient(config.BaseUrl, config.User, config.Password, true)

	cmd, args := args[0], args[1:]
	switch cmd {
	case "system-status":
		SystemStatus(client)
	case "import":
		Import(client, &config)
	default:
		log.Fatal().Msg(fmt.Sprintf("invalid subcommand %q", cmd))
	}

}

func SystemStatus(client *geoserver.GeoserverClient) {
	var err error
	var resp *geoserver.SystemStatus
	if resp, err = client.GetSystemStatus(); err != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("failed to get system status: %s", err.Error()))
	}
	log.Debug().Msg(fmt.Sprintf("status = %+v", resp))
	if j, err := json.Marshal(&resp); err != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("failed to stringify JSON: %s", err))
	} else {
		os.Stdout.Write(j)
	}
}

func Import(client *geoserver.GeoserverClient, config *Config) {
	var err error
	var imp *geoserver.Import
	var layer *geoserver.Layer

	f, ferr := os.Open(config.Filename)
	if ferr != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("failed to open import file: %s", ferr.Error()))
	}

	if layer, imp, err = client.Import(config.Workspace, f, config.Layer, config.Style, config.SrsTransform, config.Anonymous); err != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("import failed: %s", err.Error()))
	}
	log.Debug().Msg(fmt.Sprintf("import = %+v", imp))
	log.Debug().Msg(fmt.Sprintf("layer = %+v", layer))
	if j, err := json.Marshal(&layer); err != nil {
		log.Fatal().Err(err).Msg(fmt.Sprintf("failed to stringify JSON: %s", err))
	} else {
		os.Stdout.Write(j)
	}
}
